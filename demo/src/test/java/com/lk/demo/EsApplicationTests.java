package com.lk.demo;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.JSONArray;
import com.lk.demo.entity.Scholar;
import com.lk.demo.entity.User;
import com.lk.demo.service.PaperService;
import com.lk.demo.service.ScholarService;
import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.elasticsearch.action.admin.indices.delete.DeleteIndexRequest;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.support.master.AcknowledgedResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.client.indices.CreateIndexRequest;
import org.elasticsearch.client.indices.CreateIndexResponse;
import org.elasticsearch.client.indices.GetIndexRequest;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.MatchQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.reindex.DeleteByQueryRequest;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.sort.SortOrder;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SpringBootTest
class EsApplicationTests {

    @Autowired
    @Qualifier("restHighLevelClient")
    private RestHighLevelClient client;
    @Autowired
    private PaperService paperService;
    @Autowired
    private ScholarService scholarService;
    @Test
    void addIndice() throws IOException {
        CreateIndexRequest request = new CreateIndexRequest("venue");
//        request.settings(Settings.builder().put("index.number_of_shards",5));
        CreateIndexResponse response = client.indices().create(request, RequestOptions.DEFAULT);
        System.out.println(response.isAcknowledged());
    }
    @Test
    void deleteIndice() throws IOException {
//        CreateIndexRequest request = new CreateIndexRequest("paper");
//        CreateIndexResponse response = client.indices().create(request, RequestOptions.DEFAULT);
//        System.out.println(response.isAcknowledged());
        DeleteIndexRequest request = new DeleteIndexRequest().indices("venue");
        try {

            AcknowledgedResponse response = client.indices().delete(request, RequestOptions.DEFAULT);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

//    @Test
//    void addIndex() throws IOException {
//        BulkRequest request = new BulkRequest();
//        try {
//            File file = new File("F:\\newPaper\\paper01.txt");
//            InputStreamReader isr = new InputStreamReader(new FileInputStream(file), StandardCharsets.UTF_8);
//            BufferedReader br = new BufferedReader(isr);
//            String lineTxt;
//            int i=0;
//            while ((lineTxt = br.readLine()) != null) {
//                //lineTxt = new String(lineTxt.getBytes(), "utf-8");
//                JSONObject object = new JSONObject(lineTxt);
//                String data = object.toString();
////                System.out.println(data);
//                request.add(new IndexRequest().index("paper").id(object.getString("id")).source(data, XContentType.JSON));
//                i++;
//                if(i%3000==0){
//                    long a = System.currentTimeMillis();
//                    client.bulk(request,RequestOptions.DEFAULT);
//                    request = new BulkRequest();
//                    System.out.println((System.currentTimeMillis()-a));
//                    System.out.println("第"+(i/3000)+"次提交");
//                }
//            }
//            br.close();
////            System.out.println(response.getTook());
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//    }
//
//    @Test
//    void updateIndex() throws IOException {
//        BulkRequest request = new BulkRequest();
//        try {
//            File file = new File("D:/ShareCache/许天识_19373075/软工2/aminer_papers_0.txt");
//            InputStreamReader isr = new InputStreamReader(new FileInputStream(file), StandardCharsets.UTF_8);
//            BufferedReader br = new BufferedReader(isr);
//            String lineTxt;
//            int i=0;
//            while ((lineTxt = br.readLine()) != null) {
//                //lineTxt = new String(lineTxt.getBytes(), "utf-8");
//                JSONObject object = new JSONObject(lineTxt);
//                String data = object.toString();
////                System.out.println(data);
//                request.add(new IndexRequest().index("paper").id(object.getString("id")).source(data, XContentType.JSON));
//                i++;
//                if(i%3000==0){
//                    long a = System.currentTimeMillis();
//                    client.bulk(request,RequestOptions.DEFAULT);
//                    request = new BulkRequest();
//                    System.out.println((System.currentTimeMillis()-a)/100000000L);
//                    System.out.println("第"+(i/3000)+"次提交");
//                }
//                if(i%1000000==0){
//                    break;
//                }
//            }
//            br.close();
////            System.out.println(response.getTook());
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//    }
//
//    @Test
//    void searchIndex() throws IOException {
//        try {
//            SearchRequest request = new SearchRequest();
//            request.indices("paper");
//            /*查询所有*/
////            request.source(new SearchSourceBuilder().query(QueryBuilders.matchAllQuery()));
//            /*条件查询*/
//            SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
//            //分页查询
//            sourceBuilder.size(10);
//            sourceBuilder.from(100);
//            //排序
//            sourceBuilder.sort("year", SortOrder.DESC);
//            //字段过滤
//            String[] includes = {};
//            String[] excludes = {"venue","keywords","id"};
////            sourceBuilder.fetchSource(includes,excludes);
//            MatchQueryBuilder query = QueryBuilders.matchQuery("authors.org.keyword", "university");
//            BoolQueryBuilder queryBuilder = QueryBuilders.boolQuery().should(QueryBuilders.matchQuery("authors.org", "beihang"));
//            queryBuilder.must(QueryBuilders.matchQuery("authors.org", "university"));
//            queryBuilder.should(QueryBuilders.matchQuery("authors.org", "peking"));
//            sourceBuilder.query(queryBuilder);
//
//            request.source(sourceBuilder);
//            SearchResponse response = client.search(request,RequestOptions.DEFAULT);
//            SearchHits hits = response.getHits();
//
//            int i=0;
//            for(SearchHit hit:hits){
//                JSONObject object = new JSONObject(hit.getSourceAsString());
//                try {
//                    System.out.println(object.getJSONArray("authors"));
//                }
//                catch (Exception e){
//
//                }
//                i++;
//            }
//            System.out.println("命中"+i+"条");
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//    }
//
//    @Test
//    void addIndexs() throws IOException {
//        BulkRequest request = new BulkRequest();
//        try {
//            File file = new File("F:/newPaper/paper2-01.txt");
//            InputStreamReader isr = new InputStreamReader(new FileInputStream(file), StandardCharsets.UTF_8);
//            BufferedReader br = new BufferedReader(isr);
//            String lineTxt;
//            int i=0;
//            while ((lineTxt = br.readLine()) != null) {
//                //lineTxt = new String(lineTxt.getBytes(), "utf-8");
//                JSONObject object = new JSONObject(lineTxt);
//                String data = object.toString();
////                System.out.println(data);
//                request.add(new IndexRequest().index("paper").id(object.getString("id")).source(data, XContentType.JSON));
//                i++;
//                if(i%1000==0){
//                    long a = System.currentTimeMillis();
//                    client.bulk(request,RequestOptions.DEFAULT);
//                    request = new BulkRequest();
//                    System.out.println((System.currentTimeMillis()-a));
//                    System.out.println("第"+(i/1000)+"次提交");
//                    break;
//                }
//            }
//            long a = System.currentTimeMillis();
//            client.bulk(request,RequestOptions.DEFAULT);
//            System.out.println((System.currentTimeMillis()-a));
//            System.out.println("第"+(i/1000)+"次提交");
//            br.close();
////            System.out.println(response.getTook());
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//    }

//    @Test
//    void deleteIndex() throws IOException {
//        SearchRequest request = new SearchRequest();
//        try {
//            request.indices("paper");
//            SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
//            sourceBuilder.query(QueryBuilders.rangeQuery("year").gte(2023));
//            sourceBuilder.size(10000);
//            request.source(sourceBuilder);
//            SearchResponse response = client.search(request,RequestOptions.DEFAULT);
//            SearchHits hits = response.getHits();
//            System.out.println(hits.getTotalHits().value);
//            for(SearchHit hit:hits) {
//                JSONObject object = new JSONObject(hit.getSourceAsString());
//                String id = object.getString("id");
//                DeleteRequest deleteRequest = new DeleteRequest();
//                deleteRequest.index("paper").id(id);
//                client.delete(deleteRequest, RequestOptions.DEFAULT);
//            }
//            System.out.println(response.getTook());
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//    }

    @Test
    void testfind() throws IOException{
        ArrayList<String> ids = new ArrayList<>();
        ids.add("561651");
        ids.add("53e99e61b7602d97027281cd");
        ids.add("16151we");
        JSONArray a = paperService.getPaperByIDs(ids);
        System.out.println(a);
    }

    @Test
    void getavatarByAuthorId() {
        ArrayList<String> IDS = new ArrayList<>();
        IDS.add("542ef21adabfae4a9e4996f2");
        System.out.println(scholarService.getavatarByAuthorId(IDS));
    }

    public String readJsonFile(String fileName) {
        String jsonStr = "";
        try {
            File jsonFile = new File(fileName);
            FileReader fileReader = new FileReader(jsonFile);
            Reader reader = new InputStreamReader(new FileInputStream(jsonFile),"utf-8");
            int ch = 0;
            StringBuffer sb = new StringBuffer();
            while ((ch = reader.read()) != -1) {
                sb.append((char) ch);
            }
            fileReader.close();
            reader.close();
            jsonStr = sb.toString();
            return jsonStr;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Test
    void testjson() {
        String path = "D:\\大三课件\\软件系统分析\\数据采集2-文献\\文献-天文学类-550.json";
        try {
            //转换为文件
            String str2 = readJsonFile(path);
            JSONArray jsonArray =JSONArray.parseArray(str2);
            JSONArray papers = new JSONArray();
            for(int i=0;i<jsonArray.size();i++){
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                Map<String,Object> paper = new HashMap<>();
                paper.put("title",jsonObject.getString("文献名"));
                ArrayList<String> urls = new ArrayList<>();
                urls.add(jsonObject.getString("文献链接"));
                paper.put("url",urls);
                String[] authors;
                authors=jsonObject.getString("作者").split(";");
                paper.put("authors",authors);
                paper.put("year",2021);
                paper.put("n_ciation",jsonObject.getString("被引次数"));
                ArrayList<String> field = new ArrayList<>();
                field.add("astro-ph.CO");
                paper.put("field",field);
                papers.add(new JSONObject(paper));
            }
            System.out.println(papers);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
