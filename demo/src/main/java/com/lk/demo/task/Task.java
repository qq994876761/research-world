package com.lk.demo.task;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.lk.demo.controller.HotIndexController;
import com.lk.demo.entity.Scholar;
import com.lk.demo.entity.ViewField;
import com.lk.demo.entity.ViewPaper;
import com.lk.demo.entity.ViewScholar;
import com.lk.demo.mapper.ScholarMapper;
import com.lk.demo.mapper.ViewFieldMapper;
import com.lk.demo.mapper.ViewPaperMapper;
import com.lk.demo.mapper.ViewScholarMapper;
import com.lk.demo.service.PaperService;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.*;

@Component
public class Task {
    @Resource
    private ScholarMapper scholarMapper;
    @Resource
    private ViewFieldMapper viewFieldMapper;
    @Resource
    private ViewPaperMapper viewPaperMapper;
    @Resource
    private ViewScholarMapper viewScholarMapper;
    @Resource
    private PaperService paperService;

    @Scheduled(cron = "0 0 0 * * ?")
    public void task01() throws InterruptedException {
        Date time = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(time);
        calendar.add(Calendar.DATE, -7);
        time = calendar.getTime();
        QueryWrapper<ViewField> fieldWrapper=new QueryWrapper<>();
        fieldWrapper.lt("time",time);
        viewFieldMapper.delete(fieldWrapper);
        QueryWrapper<ViewPaper> paperWrapper=new QueryWrapper<>();
        paperWrapper.lt("time",time);
        viewPaperMapper.delete(paperWrapper);
        QueryWrapper<ViewScholar> scholarWrapper=new QueryWrapper<>();
        scholarWrapper.lt("time",time);
        viewScholarMapper.delete(scholarWrapper);
    }

}
