package com.lk.demo.service.impl;

import com.lk.demo.service.IAffiliationService;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

@Primary
@Service
public class AffiliationServiceImpl implements IAffiliationService {
} 