package com.lk.demo.service;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.elasticsearch.ElasticsearchException;
import org.elasticsearch.ElasticsearchStatusException;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.get.MultiGetItemResponse;
import org.elasticsearch.action.get.MultiGetRequest;
import org.elasticsearch.action.get.MultiGetResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.support.WriteRequest;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.TermQueryBuilder;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.springframework.data.util.Pair;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

public class CalRelationService extends Thread{
    private static CalRelationService cal = new CalRelationService();
    private boolean run = false;
    private RestHighLevelClient client;
    private static int page = 0;
    private static final File file = new File("./relation.log");

    public static CalRelationService getCal(RestHighLevelClient client){
        if(cal.run)
            return null;
        cal.client = client;
        return cal;
    }

    public static int getPage(){
        return page;
    }

    private com.alibaba.fastjson.JSONObject getPaperByID(String ID){
        try {
            SearchRequest request = new SearchRequest();
            SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
            request.indices("paper");
            TermQueryBuilder term = QueryBuilders.termQuery("id", ID);
            sourceBuilder.query(term);
            request.source(sourceBuilder);
            SearchResponse response = client.search(request, RequestOptions.DEFAULT);
            SearchHits hits = response.getHits();
            if(hits.getTotalHits().value==0){
                return null;
            }
            SearchHit hit = hits.getAt(0);
            return new com.alibaba.fastjson.JSONObject(hit.getSourceAsMap());
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }

    private JSONObject getAuthorObject(String ID) {
        if(ID == null)
            return null;
        SearchRequest sr = new SearchRequest();
        sr.indices("author");

        TermQueryBuilder term = QueryBuilders.termQuery("id", ID);
        QueryBuilder query = QueryBuilders.boolQuery().must(term);
        SearchSourceBuilder ssb = new SearchSourceBuilder();
        ssb.query(query);
        ssb.from(0);
        ssb.size(2);
        ssb.timeout(new TimeValue(10, TimeUnit.SECONDS));
        sr.source(ssb);
        SearchResponse result = null;
        try {
            result = client.search(sr, RequestOptions.DEFAULT);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        SearchHits hits = result.getHits();
        if(hits.getTotalHits().value == 0)
            return null;
        return new JSONObject(hits.getAt(0).getSourceAsMap());
    }

    public void run() {
        run = true;
        while(true) {
            if(this.isInterrupted())
                break;
            BulkRequest request = new BulkRequest();
            SearchRequest sr = new SearchRequest();
            sr.indices("author");
            QueryBuilder query = QueryBuilders.matchAllQuery();
            SearchSourceBuilder ssb = new SearchSourceBuilder();
            ssb.query(query);
            ssb.from(page);
            if(page%20==0){
                System.out.println(page);
            }
            if(page>=32299){
                break;
            }
            ssb.size(500);
            ssb.timeout(new TimeValue(10, TimeUnit.SECONDS));
            sr.source(ssb);
            SearchResponse result = null;

            try {
                result = client.search(sr, RequestOptions.DEFAULT);
            } catch (Exception e) {
                e.printStackTrace();
                break;
            }
            ArrayList<Pair<String,HashMap<String,JSONObject>>> mapList = new ArrayList<>();
            ArrayList<Pair<String,Integer>>pubList = new ArrayList<>();

            MultiGetRequest paperRequest = new MultiGetRequest();
//            long a = System.currentTimeMillis();
//            System.out.println(result.getHits().getAt(0).getSourceAsString());
            for(SearchHit hit:result.getHits().getHits()) {
                int cnt = 0;
                JSONObject author = new JSONObject(hit.getSourceAsMap());
                String id = author.getString("id");
                JSONArray pubs = author.getJSONArray("pubs");
                if (pubs == null || pubs.isEmpty())
                    continue;
                for (int i = 0; i < pubs.size(); ++i) {
                    JSONObject obj = pubs.getJSONObject(i);
                    if (!obj.containsKey("i"))
                        continue;
                    ++cnt;
                    paperRequest.add(new MultiGetRequest.Item("paper", obj.getString("i")));
                }
                if(cnt <=0)
                    continue;
                pubList.add(Pair.of(id,cnt));
            }
//            System.out.println(paperRequest.getItems().isEmpty());
////            System.out.println("time1:"+(System.currentTimeMillis()-a));
            if(paperRequest.getItems().isEmpty())
                continue;
            MultiGetResponse paperResponse = null;
            try{
                paperResponse = client.mget(paperRequest,RequestOptions.DEFAULT);
            }catch(Exception e){
                e.printStackTrace();
                break;
            }
//            System.out.println(paperResponse);
            if(paperResponse == null)
                break;

            String id = null;
            int cnt = 0;
            HashMap<String,JSONObject>authorMap = null;

            MultiGetRequest authorRequest = new MultiGetRequest();
//            long aa = System.currentTimeMillis();
            for(MultiGetItemResponse item: paperResponse.getResponses()) {
                if (item.getFailure() != null) {
                    Exception e = item.getFailure().getFailure();
                    ElasticsearchException ee = (ElasticsearchException) e;
                    if (ee.getMessage().contains("reason=no such index"))
                        continue;
                }
                GetResponse getResponse = item.getResponse();
                while (cnt <= 0 && !pubList.isEmpty()) {
                    id = pubList.get(0).getFirst();
                    cnt = pubList.get(0).getSecond();
                    pubList.remove(0);
                    authorMap = new HashMap<>();
                }
                if (getResponse.isExists()) {
                    JSONObject paper = new JSONObject(getResponse.getSourceAsMap());
                    JSONArray authors = paper.getJSONArray("authors");
                    if (authors == null || authors.isEmpty())
                        continue;
                    for (int j = 0; j < authors.size(); ++j) {
                        JSONObject paperAuthor = authors.getJSONObject(j);
                        if (!paperAuthor.containsKey("id"))
                            continue;
                        if (paperAuthor.getString("id").equals(id))
                            continue;
                        assert authorMap != null;
                        if (authorMap.containsKey(paperAuthor.getString("id"))) {
                            JSONObject elem = authorMap.get(paperAuthor.getString("id"));
                            int num = elem.getInteger("times") + 1;
                            elem.put("times", num);
                        } else {
                            com.alibaba.fastjson.JSONObject elem = new com.alibaba.fastjson.JSONObject();
                            elem.put("id", paperAuthor.getString("id"));
                            elem.put("name", paperAuthor.getString("name"));
                            elem.put("times", 1);
                            authorMap.put(paperAuthor.getString("id"), elem);
                            authorRequest.add(new MultiGetRequest.Item("author", paperAuthor.getString("id")));
                        }
                    }
                }
                cnt--;
                if(cnt <= 0) {
                    assert authorMap != null;
                    if (!authorMap.isEmpty()) {
                        assert id != null;
                        mapList.add(Pair.of(id, authorMap));
                    }
                }
            }
////            System.out.println("time2:"+(System.currentTimeMillis()-aa));
            MultiGetResponse authorResponse = null;

            if(authorRequest.getItems().isEmpty()){
//                System.out.println("OK1");
                continue;
            }

            try{
                authorResponse = client.mget(authorRequest,RequestOptions.DEFAULT);
            }catch(Exception e){
                e.printStackTrace();
                break;
            }
            if(authorResponse == null)
                break;

            id = null;
            authorMap = null;
            cnt = 0;
//            long aaa = System.currentTimeMillis();
            for(MultiGetItemResponse auItem: authorResponse.getResponses()) {
                if (auItem.getFailure() != null) {
                    Exception e = auItem.getFailure().getFailure();
                    ElasticsearchException ee = (ElasticsearchException) e;
                    if (ee.getMessage().contains("reason=no such index"))
                        continue;
                }
                GetResponse auResponse = auItem.getResponse();
                while(cnt<=0&&!mapList.isEmpty()) {
                    id = mapList.get(0).getFirst();
                    authorMap = mapList.get(0).getSecond();
                    cnt = authorMap.size();
                    mapList.remove(0);
                }
                if (!auResponse.isExists()) {
                    assert authorMap != null;
                    authorMap.remove(auResponse.getId());
                }
                cnt--;
                if(cnt<=0) {
                    JSONArray relation = new JSONArray();
                    assert authorMap != null;
                    relation.addAll(authorMap.values());
                    com.alibaba.fastjson.JSONObject obj = new com.alibaba.fastjson.JSONObject();
                    obj.put("relation", relation);

                    UpdateRequest updateRequest = new UpdateRequest("author", id);
                    updateRequest.doc(obj.toJSONString(), XContentType.JSON);
                    request.add(updateRequest);
                }
            }
////            System.out.println("time3:"+(System.currentTimeMillis()-aaa));
//            request.setRefreshPolicy(WriteRequest.RefreshPolicy.IMMEDIATE);
//            long aaaa = System.currentTimeMillis();
            try {
                client.bulk(request, RequestOptions.DEFAULT);
            } catch (Exception e) {
                e.printStackTrace();
                break;
            }
////            System.out.println("time4:"+(System.currentTimeMillis()-aaaa));
            page++;
        }
        cal = new CalRelationService();
        run=false;
    }
}
