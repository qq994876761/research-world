package com.lk.demo.service;

import com.lk.demo.entity.Scholar;

import java.util.List;

public interface IScholarService {
    List<Scholar> selectScholarByIds(List<String> authorIds);
    List<Scholar> selectScholarByIdsNew(List<String> authorIds);
} 