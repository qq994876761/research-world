package com.lk.demo.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.lk.demo.entity.Scholar;
import com.lk.demo.mapper.ScholarMapper;
import com.lk.demo.service.IScholarService;
import io.swagger.models.auth.In;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@Primary
@Service
public class ScholarServiceImpl implements IScholarService {
    @Resource
    ScholarMapper scholarMapper;

    @Override
    public List<Scholar> selectScholarByIds(List<String> authorIds) {
        List<Scholar> scholars = new ArrayList<>();
        for(String authorId:authorIds){
            QueryWrapper<Scholar> wrapper = new QueryWrapper<>();
            wrapper.eq("AuthorId", authorId);
            Scholar scholar = scholarMapper.selectOne(wrapper);
            scholars.add(scholar);
        }
        return scholars;
    }

    @Override
    public List<Scholar> selectScholarByIdsNew(List<String> authorIds) {
        List<Scholar> scholars = new ArrayList<>();
        for(String authorId:authorIds){
            Scholar scholar = scholarMapper.selectByAuthorId(authorId);
            scholars.add(scholar);
        }
        return scholars;
    }
}