package com.lk.demo.service;

public interface IHotIndexService {
    void viewField(String fieldId) ;

    void viewPaper(String PaperId,String field);

    void viewScholar(Integer scholarId);
}
