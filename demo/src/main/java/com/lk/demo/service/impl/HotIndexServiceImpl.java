package com.lk.demo.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.lk.demo.entity.ViewField;
import com.lk.demo.entity.ViewPaper;
import com.lk.demo.entity.ViewScholar;
import com.lk.demo.mapper.ViewFieldMapper;
import com.lk.demo.mapper.ViewPaperMapper;
import com.lk.demo.mapper.ViewScholarMapper;
import com.lk.demo.service.IHotIndexService;
import com.lk.demo.service.PaperService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;

@Primary
@Service
public class HotIndexServiceImpl implements IHotIndexService {
    @Resource
    private ViewFieldMapper viewFieldMapper;
    @Resource
    private ViewPaperMapper viewPaperMapper;
    @Resource
    private ViewScholarMapper viewScholarMapper;
    private final PaperService paperService;

    public HotIndexServiceImpl(PaperService paperService) {
        this.paperService = paperService;
    }

    public void viewField(String fieldId) {
        ViewField viewField = new ViewField();
        viewField.setFieldId(fieldId);
        viewFieldMapper.insert(viewField);
    }

    public void viewPaper(String paperId,String field) {
        ViewPaper viewPaper = new ViewPaper();
        paperService.visit(paperId);
        viewPaper.setField(field);
        viewPaper.setPaperId(paperId);
        viewPaperMapper.insert(viewPaper);
    }

    public void viewScholar(Integer scholarId) {
        ViewScholar viewScholar = new ViewScholar();
        viewScholar.setScholarId(scholarId);
        viewScholarMapper.insert(viewScholar);
    }
}
