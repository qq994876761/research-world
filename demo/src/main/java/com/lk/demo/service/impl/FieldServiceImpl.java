package com.lk.demo.service.impl;

import com.lk.demo.service.IFieldService;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

@Primary
@Service
public class FieldServiceImpl implements IFieldService {
} 