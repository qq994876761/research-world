package com.lk.demo.service;


import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.lk.demo.common.R;
import com.lk.demo.entity.Scholar;
import com.lk.demo.entity.ScholarPortal;
import com.lk.demo.entity.User;
import com.lk.demo.mapper.ScholarMapper;
import com.lk.demo.mapper.ScholarPortalMapper;
import com.lk.demo.mapper.UserMapper;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import java.util.*;

@Service
public class ScholarService {
    @Resource
    private ScholarMapper scholarMapper;

    @Resource
    private UserMapper userMapper;

    @Resource
    private ScholarPortalMapper scholarPortalMapper;

    //@ApiOperation(notes = "无需登录，参数为作者id列表：ids", value = "根据作者id获取学者和头像")
    public ArrayList<HashMap<String,Object>> getavatarByAuthorId(ArrayList<String> ids) {
        try {
            ArrayList<HashMap<String,Object>> maps=new ArrayList<>();
            ArrayList<Scholar> scholars = new ArrayList<>();
            for (String id : ids) {
                HashMap<String,Object> map = new HashMap<String,Object>();
                System.out.println(id);
                Scholar scholar = scholarMapper.selectByAuthorId(id);
                if(scholar==null){
                    map.put("scholar",null);
                    map.put("avatar", null);
                    maps.add(map);
                    continue;
                }
                map.put("scholar",scholar);
                User user=userMapper.selectById(scholar.getUserId());
                map.put("avatar",user.getAvatar());
                maps.add(map);
            }
            return maps;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }


    //封禁用户，type为0则为解封，type为1为禁言，type为2为封号，date为解封日期，type为0时用不着
    public void banuser(Date date,Integer state,Integer userid){
        try {
            User user=userMapper.selectById(userid);
            if(state==0){
                user.setState(0);
                user.setBanDate(null);
            }else if(state==1){
                user.setState(1);
                user.setBanDate(date);
            }else if(state==2){
                user.setState(2);
                user.setBanDate(date);
            }
            userMapper.updateById(user);
            return;
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }
    }


}



