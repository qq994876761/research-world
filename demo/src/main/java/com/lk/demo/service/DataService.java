package com.lk.demo.service;

import org.elasticsearch.ElasticsearchStatusException;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.search.MultiSearchRequest;
import org.elasticsearch.action.search.MultiSearchResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.support.WriteRequest;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.TermQueryBuilder;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@Service
public class DataService {
    private final RestHighLevelClient client;

    public DataService(@Qualifier("restHighLevelClient") RestHighLevelClient client) {
        this.client = client;
    }

    public void addIndexs(){
        BulkRequest request = new BulkRequest();
        try {
            for(int j=1;j<=7;j++){
                File file = new File("/mnt/sdc/Alldata/paper0"+j+".txt");
                InputStreamReader isr = new InputStreamReader(new FileInputStream(file), StandardCharsets.UTF_8);
                BufferedReader br = new BufferedReader(isr);
                String lineTxt;
                int i=0;
                while ((lineTxt = br.readLine()) != null) {
                    //lineTxt = new String(lineTxt.getBytes(), "utf-8");
                    org.json.JSONObject object = new org.json.JSONObject(lineTxt);
                    String data = object.toString();
//                System.out.println(data);
                    request.add(new IndexRequest().index("paper").id(object.getString("id")).source(data, XContentType.JSON));
                    i++;
                    if(i%1000==0){
                        long a = System.currentTimeMillis();
                        client.bulk(request, RequestOptions.DEFAULT);
                        request = new BulkRequest();
                        System.out.println((System.currentTimeMillis()-a));
                        System.out.println("第"+(i/1000)+"次提交");
                    }
                }
                long a = System.currentTimeMillis();
                client.bulk(request, RequestOptions.DEFAULT);
                request = new BulkRequest();
                System.out.println((System.currentTimeMillis()-a));
                System.out.println("第"+(i/1000)+"次提交");
                br.close();
            }
//            System.out.println(response.getTook());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public int calRelation(){
        int page = CalRelationService.getPage();
        CalRelationService cal = CalRelationService.getCal(client);
        if(cal != null)
            cal.start();
        return page;
    }

    public void addauthors(Integer from, Integer to){
        BulkRequest request = new BulkRequest();
        try {
            for(int j=from;j<=to;j++){
                File file = new File("/mnt/sdc/Alldata/author/aminer_authors_"+j+".txt");
                InputStreamReader isr = new InputStreamReader(new FileInputStream(file), StandardCharsets.UTF_8);
                BufferedReader br = new BufferedReader(isr);
                String lineTxt;
                MultiSearchRequest msRequest = new MultiSearchRequest();
                org.json.JSONArray jsonArray = new org.json.JSONArray();
                int i=1,k=0,l=0;
                while ((lineTxt = br.readLine()) != null) {
                    k++;
                    if(k%10000==0){
                        System.out.println("k:"+k);
                    }
                    //lineTxt = new String(lineTxt.getBytes(), "utf-8");
                    org.json.JSONObject object = new org.json.JSONObject(lineTxt);
                    l++;
                    //
                    jsonArray.put(object);
                    String id = object.getString("id");
                    SearchRequest request1 = new SearchRequest("paper");
                    SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
                    sourceBuilder.query(QueryBuilders.matchQuery("authors.id", id));
                    request1.source(sourceBuilder);
                    msRequest.add(request1);
                    if(l==500){
                        try {
                            int kk=0;
                            MultiSearchResponse multiResponse = client.msearch(msRequest, RequestOptions.DEFAULT);
                            for (MultiSearchResponse.Item item : multiResponse.getResponses()) {
                                SearchHits hits = item.getResponse().getHits();
                                if (hits.getTotalHits().value!=0) {
                                    i++;
                                    Map<String,Object> map = jsonArray.getJSONObject(kk).toMap();
                                    map.put("relation", new ArrayList<>());
                                    org.json.JSONObject object1 = new org.json.JSONObject(map);
                                    String data = object1.toString();
                                    request.add(new IndexRequest().index("author").id(object1.getString("id")).source(data, XContentType.JSON));
                                }
                                kk++;
                            }
                            jsonArray = new JSONArray();
                            msRequest = new MultiSearchRequest();
                            if(i>=500){
                                long a = System.currentTimeMillis();
                                client.bulk(request, RequestOptions.DEFAULT);
                                request = new BulkRequest();
                                System.out.println((System.currentTimeMillis()-a));
                                i=0;
                            }
                            l=0;
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
                if(msRequest.requests().size()!=0){
                    try {
                        int kk=0;
                        MultiSearchResponse multiResponse = client.msearch(msRequest, RequestOptions.DEFAULT);
                        for (MultiSearchResponse.Item item : multiResponse.getResponses()) {
                            SearchHits hits = item.getResponse().getHits();
                            if (hits.getTotalHits().value!=0) {
                                i++;
                                Map<String, Object> map = jsonArray.getJSONObject(kk).toMap();
                                map.put("relation", new ArrayList<>());
                                org.json.JSONObject object1 = new JSONObject(map);
                                String data = object1.toString();
                                request.add(new IndexRequest().index("author").id(object1.getString("id")).source(data, XContentType.JSON));
                            }
                            kk++;
                        }
                        long a = System.currentTimeMillis();
                        client.bulk(request, RequestOptions.DEFAULT);
                        request = new BulkRequest();
                        System.out.println((System.currentTimeMillis()-a));
                        System.out.println("nowcomplete:"+j);
                        continue;
                    } catch (IOException e) {
                        continue;
                    }
                }
                if(request.requests().size()!=0){
                    try {
                        long a = System.currentTimeMillis();
                        client.bulk(request, RequestOptions.DEFAULT);
                        request = new BulkRequest();
                        System.out.println((System.currentTimeMillis()-a));
                        System.out.println("nowcomplete:"+j);
                    }
                    catch (Exception e){
                        continue;
                    }
                }
                System.out.println("nowcomplete:"+j);
                br.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void addVenue(){
        BulkRequest request = new BulkRequest();
        try {
            File file = new File("/mnt/sdc/Alldata/author/aminer_venues.txt");
//                File file = new File("F:/newPaper/paper01.txt");
            InputStreamReader isr = new InputStreamReader(new FileInputStream(file), StandardCharsets.UTF_8);
            BufferedReader br = new BufferedReader(isr);
            String lineTxt;
            int i=0;
            while ((lineTxt = br.readLine()) != null) {
                //lineTxt = new String(lineTxt.getBytes(), "utf-8");
                JSONObject object = new JSONObject(lineTxt);
                String id = object.getString("id");
                SearchRequest searchRequest = new SearchRequest();
                searchRequest.indices("paper");
                SearchSourceBuilder sourceBuilder1 = new SearchSourceBuilder();
                sourceBuilder1.query(QueryBuilders.matchQuery("venue.id", id));
                searchRequest.source(sourceBuilder1);
                SearchResponse response1 = client.search(searchRequest, RequestOptions.DEFAULT);
                SearchHits hits1 = response1.getHits();
                if (hits1.getTotalHits().value != 0) {
                    Map<String,Object> map = object.toMap();
                    map.put("paper_num",0);
                    map.put("cite_num",0);
                    object = new JSONObject(map);
                    String data = object.toString();
//                System.out.println(data);
                    request.add(new IndexRequest().index("venue").id(object.getString("id")).source(data, XContentType.JSON));
                    i++;
                }
                if(i==1000){
                    i=0;
                    long a = System.currentTimeMillis();
                    client.bulk(request, RequestOptions.DEFAULT);
                    request = new BulkRequest();
                    System.out.println((System.currentTimeMillis()-a));
                }
            }
            try {
                long a = System.currentTimeMillis();
                client.bulk(request, RequestOptions.DEFAULT);
                System.out.println((System.currentTimeMillis()-a));
            }
            catch (Exception e){
                System.out.println("over");
            }
            System.out.println("over!");
            br.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
