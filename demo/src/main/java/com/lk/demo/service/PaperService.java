package com.lk.demo.service;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.lk.demo.entity.Scholar;
import com.lk.demo.entity.ScholarPortal;
import com.lk.demo.entity.wrapper.paper.OneCondition;
import com.lk.demo.entity.wrapper.paper.PaperFilter;
import com.lk.demo.entity.wrapper.paper.ReturnPapers;
import com.lk.demo.entity.wrapper.paper.Year;
import com.lk.demo.mapper.ScholarMapper;
import com.lk.demo.mapper.ScholarPortalMapper;
import org.elasticsearch.ElasticsearchException;
import org.elasticsearch.ElasticsearchStatusException;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.get.MultiGetItemResponse;
import org.elasticsearch.action.get.MultiGetRequest;
import org.elasticsearch.action.get.MultiGetResponse;
import org.elasticsearch.action.search.MultiSearchResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.action.update.UpdateResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.text.Text;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.*;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightField;
import org.elasticsearch.search.sort.SortOrder;
import org.elasticsearch.search.suggest.Suggest;
import org.elasticsearch.search.suggest.SuggestBuilder;
import org.elasticsearch.search.suggest.completion.CompletionSuggestion;
import org.elasticsearch.search.suggest.completion.CompletionSuggestionBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import javax.annotation.Resource;
import java.util.*;
import java.util.concurrent.TimeUnit;

@Service
public class PaperService {
    private final ScholarService scholarService;
    private final RestHighLevelClient client;
    @Resource
    private ScholarPortalMapper scholarPortalMapper;
    @Resource
    private ScholarMapper scholarMapper;
    public String[] fields = {"title","abstract"};
    public HighlightBuilder highlightBuilder;
    {
        highlightBuilder = new HighlightBuilder();
        //设置高亮字段
        for (String s:fields){
            highlightBuilder.field(s);
        }
        //如果要多个字段高亮,这项要为false
        highlightBuilder.requireFieldMatch(false);
        highlightBuilder.preTags("<mark>");
        highlightBuilder.postTags("</mark>");
        highlightBuilder.fragmentSize(800000); //最大高亮分片数
        highlightBuilder.numOfFragments(0); //从第一个分片获取高亮片段
    }

    public PaperService(RestHighLevelClient client, ScholarService scholarService) {
        this.client = client;
        this.scholarService = scholarService;
    }

    /*返回一个JSONObject，没有找到返回null*/
    public JSONObject getPaperByID(String ID){
        try {
            SearchRequest request = new SearchRequest();
            SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
            request.indices("paper");
            TermQueryBuilder term = QueryBuilders.termQuery("id", ID);
            sourceBuilder.query(term);
            request.source(sourceBuilder);
            SearchResponse response = client.search(request,RequestOptions.DEFAULT);
            SearchHits hits = response.getHits();
            if(hits.getTotalHits().value==0){
                return null;
            }
            SearchHit hit = hits.getAt(0);
            return new JSONObject(hit.getSourceAsMap());
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }
    /*根据Id列表返回论文数据，没有找到返回空的JSONArray*/
    public JSONArray getPaperByIDs(ArrayList<String> ids){
        try {
            if(ids.size()==0){
                return new JSONArray();
            }
            JSONArray jsonArray = new JSONArray();
            MultiGetRequest mRequest = new MultiGetRequest();
            for(String id:ids){
                mRequest.add(new MultiGetRequest.Item("paper", id));
            }
            if(mRequest.getItems().isEmpty())
                return new JSONArray();
            MultiGetResponse mResponse = client.mget(mRequest,RequestOptions.DEFAULT);
            for(MultiGetItemResponse item: mResponse.getResponses()) {
                GetResponse getResponse = item.getResponse();
                if(getResponse.isExists()){
                    JSONObject author = new JSONObject(getResponse.getSourceAsMap());
                    jsonArray.add(author);
                }
                else{
                    jsonArray.add(null);
                }

            }
//            jsonArray = JSONObject.parseArray(JSONObject.toJSONString(hits.getTotalHits().toString()));
            return jsonArray;
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }
    /*找到所有论文，参数是每页展示多少，以及页码*/
    public ReturnPapers getAllPaper(int size, int page){
        try {
            JSONArray jsonArray = new JSONArray();
            SearchRequest request = new SearchRequest();
            request.indices("paper");
            SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
            MatchAllQueryBuilder matchAllQuery = QueryBuilders.matchAllQuery();
            sourceBuilder.query(matchAllQuery);
            request.source(sourceBuilder);
            sourceBuilder.size(size);
            sourceBuilder.from(size*(page-1));
            sourceBuilder.trackTotalHits(true);
            SearchResponse response = client.search(request,RequestOptions.DEFAULT);
            SearchHits hits = response.getHits();
            if(hits.getTotalHits().value==0){
                return null;
            }
            for(SearchHit hit:hits){
                jsonArray.add(new JSONObject(hit.getSourceAsMap()));
            }
//            jsonArray = JSONObject.parseArray(JSONObject.toJSONString(hits.getTotalHits().toString()));
            ReturnPapers returnPapers = new ReturnPapers(jsonArray,hits.getTotalHits().value);
            return returnPapers;
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }

    /*单条件查询，传入查询字段必须与数据库相同，否则要switch转义很麻烦*/
    public ReturnPapers searchSimple(OneCondition condition, Year year, Integer sort,PaperFilter filter){
        try {
            SearchRequest request = new SearchRequest();
            request.indices("paper");
            SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
            BoolQueryBuilder queryBuilder = QueryBuilders.boolQuery();
            queryBuilder.must(QueryBuilders.matchQuery(condition.getKey(),condition.getValue()));
            queryBuilder.must(QueryBuilders.rangeQuery("year").from(year.getL()).to(year.getR()).includeLower(true).includeUpper(true));
            queryBuilder = filter(filter,queryBuilder);
            sourceBuilder.query(queryBuilder);
            sourceBuilder.size(condition.getSize());
            sourceBuilder.from(condition.getSize()*(condition.getPage()-1));
            sourceBuilder.trackTotalHits(true);
            sourceBuilder.highlighter(highlightBuilder);
            sourceBuilder = sort(sourceBuilder,sort);
            request.source(sourceBuilder);
            return search(request);
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }

    /*多条件查询，传入一个condition数组，传入查询字段必须与数据库相同，否则要switch转义很麻烦*/
    public ReturnPapers searchMulti(ArrayList<OneCondition> conditionList, Year year, Integer sort,PaperFilter filter){
        try {
            SearchRequest request = new SearchRequest();
            request.indices("paper");
            SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
            BoolQueryBuilder queryBuilder = QueryBuilders.boolQuery();
            queryBuilder.must(QueryBuilders.rangeQuery("year").from(year.getL()).to(year.getR()).includeLower(true).includeUpper(true));
            for(OneCondition condition:conditionList){
                switch (condition.getOption()){
                    case 0:{
                        queryBuilder.must(QueryBuilders.matchQuery(condition.getKey(),condition.getValue()));
                        break;
                    }
                    case 1:{
                        queryBuilder.should(QueryBuilders.matchQuery(condition.getKey(),condition.getValue()));
                        break;
                    }
                    case 2:{
                        queryBuilder.mustNot(QueryBuilders.matchQuery(condition.getKey(),condition.getValue()));
                        break;
                    }
                    default:{
                        return null;
                    }
                }
            }
            queryBuilder = filter(filter,queryBuilder);
            sourceBuilder.highlighter(highlightBuilder);
            sourceBuilder.query(queryBuilder);
            sourceBuilder = sort(sourceBuilder,sort);
            sourceBuilder.size(conditionList.get(0).getSize());
            sourceBuilder.from(conditionList.get(0).getSize()*(conditionList.get(0).getPage()-1));
            sourceBuilder.trackTotalHits(true);
            request.source(sourceBuilder);
            return search(request);
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }

    /*主题查询，因为没有主题字段，实际采用查询领域、篇名、keyword的方法*/
    public ReturnPapers searchByTheme(OneCondition condition, Year year, Integer sort,PaperFilter filter){
        try {
            SearchRequest request = new SearchRequest();
            request.indices("paper");
            SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
            BoolQueryBuilder queryBuilder = QueryBuilders.boolQuery();
            queryBuilder.must(QueryBuilders.rangeQuery("year").from(year.getL()).to(year.getR()).includeLower(true).includeUpper(true));
            queryBuilder.should(QueryBuilders.matchQuery("field",condition.getValue()));
            queryBuilder.should(QueryBuilders.matchQuery("title",condition.getValue()));
            queryBuilder.should(QueryBuilders.matchQuery("keywords",condition.getValue()));
            queryBuilder = filter(filter,queryBuilder);
            //高亮设置开始------
            sourceBuilder.highlighter(highlightBuilder);
            //高亮设置结束------
            sourceBuilder.query(queryBuilder);

            sourceBuilder = sort(sourceBuilder,sort);
            sourceBuilder.size(condition.getSize());
            sourceBuilder.from(condition.getSize()*(condition.getPage()-1));
            request.source(sourceBuilder);
//            jsonArray = JSONObject.parseArray(JSONObject.toJSONString(hits.getTotalHits().toString()));
            return search(request);
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }


    /*主题查询，因为没有主题字段，实际采用查询领域、篇名、keyword的方法*/
    public JSONArray getPaperByAuthorId(String ID){
        try {
            SearchRequest request = new SearchRequest();
            SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
            request.indices("paper");
            TermQueryBuilder term = QueryBuilders.termQuery("authors.id", ID);
            sourceBuilder.query(term);
            request.source(sourceBuilder);
            SearchResponse response = client.search(request,RequestOptions.DEFAULT);
            SearchHits hits = response.getHits();
            if(hits.getTotalHits().value==0){
                return null;
            }
            JSONArray jsonArray = new JSONArray();
            for(SearchHit hit:hits){
                JSONObject object = new JSONObject(hit.getSourceAsMap());
                jsonArray.add(object);
            }
            return jsonArray;
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }

    public JSONArray suggest(String prefix,String field,int size){
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        CompletionSuggestionBuilder suggestionBuilderDistrict =
                new CompletionSuggestionBuilder(field+".suggest").prefix(prefix).size(size);
        SuggestBuilder suggestBuilder = new SuggestBuilder();
        suggestBuilder.addSuggestion("my_suggest", suggestionBuilderDistrict);
        searchSourceBuilder.suggest(suggestBuilder);
        SearchRequest searchRequest = new SearchRequest();
        searchRequest.indices("paper");
        searchRequest.source(searchSourceBuilder);
        SearchResponse response = null;
        try {
            response = client.search(searchRequest, RequestOptions.DEFAULT);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        HashSet<String> suggestSet = new HashSet<>();
        int cnt =0;
        Suggest suggest = response.getSuggest();
        if(suggest != null){
            for (Object term : suggest.getSuggestion("my_suggest").getEntries()) {
                if (term instanceof CompletionSuggestion.Entry) {
                    CompletionSuggestion.Entry item = (CompletionSuggestion.Entry) term;
                    if (!item.getOptions().isEmpty()) {
                        for (CompletionSuggestion.Entry.Option option : item.getOptions()) {
                            String tip = option.getText().toString();
                            if (!suggestSet.contains(tip)) {
                                suggestSet.add(tip);
                                cnt++;
                            }
                        }
                    }
                }
                if (cnt >= size) {
                    break;
                }
            }
        }
        JSONArray ans = new JSONArray();
        ans.addAll(suggestSet);
        return ans;
    }

    public JSONArray getRelAuthor(JSONArray papers){
        ArrayList<String> ids = new ArrayList<>();
        JSONArray authors ;
        JSONArray authors2 = new JSONArray();
        for(int i=0;i<papers.size();i++){
            JSONObject paper = papers.getJSONObject(i);
            if(paper.containsKey("authors")){
                JSONArray temp = paper.getJSONArray("authors");
            if(temp.size()!=0&&temp.getJSONObject(0).getString("id")!=null){
                    ids.add(temp.getJSONObject(0).getString("id"));
                    authors2.add(temp.getJSONObject(0));
                }
            }
        }
        authors = getAuthorByIDs(ids);
        System.out.println(authors);
        JSONArray returnAuthor = new JSONArray();
        ArrayList<HashMap<String,Object>> scholars = scholarService.getavatarByAuthorId(ids);

        System.out.println(scholars);
        for(int i=0;i<ids.size();i++){
            if(scholars.get(i).get("scholar")!=null){
                returnAuthor.add(new JSONObject(scholars.get(i)));
            }
            else if(authors.get(i)!=null){
                returnAuthor.add(authors.getJSONObject(i));
            }
            else {
                returnAuthor.add(authors2.getJSONObject(i));
            }
        }
        return returnAuthor;
    }


    public JSONArray getAuthorByIDs(ArrayList<String> IDs) {
        if(IDs.size()==0){
            return new JSONArray();
        }
        MultiGetRequest mRequest = new MultiGetRequest();
        for(String ID:IDs) {
            mRequest.add(new MultiGetRequest.Item("author", ID));
        }
        if(mRequest.getItems().isEmpty())
            return new JSONArray();
        MultiGetResponse mResponse = null;
        try{
            mResponse = client.mget(mRequest,RequestOptions.DEFAULT);
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }
//            System.out.println(paperResponse);
        if(mResponse == null)
            return new JSONArray();

        JSONArray result = new JSONArray();
        for(MultiGetItemResponse item: mResponse.getResponses()) {
            GetResponse getResponse = item.getResponse();
            if(getResponse.isExists()){
                JSONObject author = new JSONObject(getResponse.getSourceAsMap());
                result.add(author);
            }
            else{
                result.add(null);
            }
        }
        return result;
    }


    /*访问了某一篇论文*/
    public boolean visit(String ID){
        UpdateRequest updateRequest = new UpdateRequest();
        updateRequest.index("paper").id(ID);


        //通过client对象执行操作
        try {
            JSONObject paper = getPaperByID(ID);
            if(paper==null){
                return false;
            }
            Integer vis_count = paper.getInteger("vis_count");
            updateRequest.doc(XContentType.JSON,"vis_count",vis_count+1);
            UpdateResponse response = client.update(updateRequest, RequestOptions.DEFAULT);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public ReturnPapers search(SearchRequest request){
        try {
            JSONArray jsonArray = new JSONArray();
            SearchResponse response = client.search(request,RequestOptions.DEFAULT);
            SearchHits hits = response.getHits();
            System.out.println(hits.getTotalHits().value);
            if(hits.getTotalHits().value==0){
                return null;
            }
            for (SearchHit hit : hits) {
                Map<String, Object> sourceAsMap = hit.getSourceAsMap();
                //解析高亮字段
                Map<String, HighlightField> highlightFields = hit.getHighlightFields();
                for(String s:fields){
                    HighlightField field= highlightFields.get(s);
                    if(field!= null){
                        Text[] fragments = field.fragments();
                        StringBuilder n_field = new StringBuilder();
                        for (Text fragment : fragments) {
                            n_field.append(fragment);
                        }
                        //高亮标题覆盖原标题
                        sourceAsMap.put(s, n_field.toString());
                    }
                }
                sourceAsMap.put("score",hit.getScore());
                jsonArray.add(new JSONObject(sourceAsMap));
            }
            JSONArray authors = getRelAuthor(jsonArray);
//            System.out.println(authors);
            return new ReturnPapers(jsonArray,hits.getTotalHits().value,authors);
        }
        catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }
    public SearchSourceBuilder sort(SearchSourceBuilder sourceBuilder,Integer sort){
        if(sort==1){
            return sourceBuilder;
        }
        switch (sort){
            case 2:{
                sourceBuilder.sort("year", SortOrder.DESC);
                break;
            }
            case 3:{
                sourceBuilder.sort("n_citation", SortOrder.DESC);
                break;
            }
            case 4:{
                sourceBuilder.sort("vis_count", SortOrder.DESC);
                break;
            }
            case -2:{
                sourceBuilder.sort("year", SortOrder.ASC);
                break;
            }
            case -3:{
                sourceBuilder.sort("n_citation", SortOrder.ASC);
                break;
            }
            case -4:{
                sourceBuilder.sort("vis_count", SortOrder.ASC);
                break;
            }
        }
        return sourceBuilder;
    }
    public BoolQueryBuilder filter(PaperFilter filter,BoolQueryBuilder queryBuilder){
        if(filter!=null){
            if(filter.isCanGet()){
                queryBuilder.must(QueryBuilders.existsQuery("url"));
            }
            if(filter.isHot()){
                queryBuilder.must(QueryBuilders.rangeQuery("vis_count").gte(50));
            }
            if(filter.isHighSite()){
                queryBuilder.must(QueryBuilders.rangeQuery("n_citation").gte(30));
            }
        }
        return queryBuilder;
    }

    //删除学者的作者id。需要学者id和作者id作为参数
    public void deleteauthorid(Integer scholarid,String authorid){
        try {
            Scholar scholar=scholarMapper.selectById(scholarid);
            scholar.deleteauthor(authorid);

            QueryWrapper<ScholarPortal> wrapper1 = new QueryWrapper<>();
            wrapper1.eq("scholar_id", scholar.getId());
            ScholarPortal scholarPortal=scholarPortalMapper.selectOne(wrapper1);

            JSONArray papers = getPaperByAuthorId(authorid);
            if(papers!=null){
                for (Object paper : papers) {
                    System.out.println(((JSONObject) paper).getString("id"));

                    scholar.deletepaper(((JSONObject) paper).getString("id"));
                    scholarPortal.deletepaper(((JSONObject) paper).getString("id"));
                }
            }
            scholarPortalMapper.updateById(scholarPortal);
            scholarMapper.updateById(scholar);


        }catch (Exception e) {
            e.printStackTrace();
            return;
        }
    }

}
