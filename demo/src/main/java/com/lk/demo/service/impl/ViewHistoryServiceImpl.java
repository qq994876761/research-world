package com.lk.demo.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.lk.demo.entity.ViewHistory;
import com.lk.demo.mapper.ViewHistoryMapper;
import com.lk.demo.service.IViewHistoryService;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Primary
@Service
public class ViewHistoryServiceImpl implements IViewHistoryService {
    @Resource
    ViewHistoryMapper viewHistoryMapper;

    @Override
    public Boolean viewHistory(Integer userId, String paperId) {
        QueryWrapper<ViewHistory> wrapper = new QueryWrapper<>();
        if (paperId == null || userId == null)
            return false;
        wrapper.eq("user_id", userId);
        wrapper.eq("paper_id", paperId);
        viewHistoryMapper.delete(wrapper);
        ViewHistory viewHistory = new ViewHistory();
        viewHistory.setUserId(userId);
        viewHistory.setPaperId(paperId);
        viewHistoryMapper.insert(viewHistory);
        return true;
    }
}