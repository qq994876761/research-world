package com.lk.demo.service;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.lk.demo.entity.wrapper.paper.ReturnPapers;
import com.lk.demo.entity.wrapper.venue.VenueName;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.text.Text;
import org.elasticsearch.index.query.*;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightField;
import org.elasticsearch.search.sort.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class VenueService {
    public String[] fields = {"NormalizedName","DisplayName"};
    public HighlightBuilder highlightBuilder;
    {
        highlightBuilder = new HighlightBuilder();
        //设置高亮字段
        for (String s:fields){
            highlightBuilder.field(s);
        }
        //如果要多个字段高亮,这项要为false
        highlightBuilder.requireFieldMatch(false);
        highlightBuilder.preTags("<span style='color:red'>");
        highlightBuilder.postTags("</span>");
        highlightBuilder.fragmentSize(800000); //最大高亮分片数
        highlightBuilder.numOfFragments(0); //从第一个分片获取高亮片段
    }
    private final RestHighLevelClient client;
    @Autowired
    public VenueService(RestHighLevelClient restHighLevelClient){
        this.client = restHighLevelClient;
    }

    /*返回一个JSONObject，没有找到返回null*/
    public JSONObject geVenueByID(String ID){
        try {
            SearchRequest request = new SearchRequest();
            SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
            request.indices("venue");
            TermQueryBuilder term = QueryBuilders.termQuery("id", ID);
            sourceBuilder.query(term);
            request.source(sourceBuilder);
            SearchResponse response = client.search(request,RequestOptions.DEFAULT);
            SearchHits hits = response.getHits();
            if(hits.getTotalHits().value==0){
                return null;
            }
            SearchHit hit = hits.getAt(0);
            return new JSONObject(hit.getSourceAsMap());
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }
    /*展示所有期刊/会议，参数是每页展示多少，以及页码*/
    public JSONArray getAllVenue(int size,int page){
        try {
            JSONArray jsonArray = new JSONArray();
            SearchRequest request = new SearchRequest();
            request.indices("venue");
            SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
            MatchAllQueryBuilder matchAllQuery = QueryBuilders.matchAllQuery();
            sourceBuilder.query(matchAllQuery);
            request.source(sourceBuilder);
            sourceBuilder.size(size);
            sourceBuilder.from(size*(page-1));
            sourceBuilder.trackTotalHits(true);
            SearchResponse response = client.search(request,RequestOptions.DEFAULT);
            SearchHits hits = response.getHits();
            if(hits.getTotalHits().value==0){
                return null;
            }
            for(SearchHit hit:hits){
                jsonArray.add(new JSONObject(hit.getSourceAsMap()));
            }
//            jsonArray = JSONObject.parseArray(JSONObject.toJSONString(hits.getTotalHits().toString()));
            return jsonArray;
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }

    /*获取会议全部论文只需要调用一般的检索即可*/
    public ReturnPapers getPaperByVenueId(String ID,Integer page,Integer size,Integer sort){
        try {
            SearchRequest request = new SearchRequest();
            request.indices("paper");
            SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
            MatchQueryBuilder queryBuilder = QueryBuilders.matchQuery("venue.id",ID);
            sourceBuilder.query(queryBuilder);
            sourceBuilder.size(size);
            sourceBuilder.from(page);
            sourceBuilder.trackTotalHits(true);
            sourceBuilder = sort(sourceBuilder,sort);
            request.source(sourceBuilder);
            return search(request);
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }

    /*根据名字检索期刊/会议*/
    public ReturnPapers getVenueByname(VenueName venueName){
        try {
            SearchRequest request = new SearchRequest();
            request.indices("venue");
            SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
            BoolQueryBuilder boolQueryBuilder = new BoolQueryBuilder();
            boolQueryBuilder.should(QueryBuilders.matchQuery("NormalizedName",venueName.getName()));
            boolQueryBuilder.should(QueryBuilders.matchQuery("DisplayName",venueName.getName()));
            sourceBuilder.query(boolQueryBuilder);
            sourceBuilder.size(venueName.getSize());
            sourceBuilder.from(venueName.getPage());
            sourceBuilder.trackTotalHits(true);
//            sourceBuilder = sort(sourceBuilder,sort);
            request.source(sourceBuilder);
            return search(request);
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }

    public ReturnPapers search(SearchRequest request){
        try {
            JSONArray jsonArray = new JSONArray();
            SearchResponse response = client.search(request,RequestOptions.DEFAULT);
            SearchHits hits = response.getHits();
//            System.out.println(hits.getTotalHits().value);
            if(hits.getTotalHits().value==0){
                return null;
            }
            for (SearchHit hit : hits) {
                jsonArray.add(new JSONObject(hit.getSourceAsMap()));
            }
            return new ReturnPapers(jsonArray,hits.getTotalHits().value);
        }
        catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }
    public SearchSourceBuilder sort(SearchSourceBuilder sourceBuilder,Integer sort){
        if(sort==1){
            return sourceBuilder;
        }
        switch (sort){
            case 2:{
                sourceBuilder.sort("year", SortOrder.DESC);
                break;
            }
            case 3:{
                sourceBuilder.sort("n_citation", SortOrder.DESC);
                break;
            }
            case 4:{
                sourceBuilder.sort("vis_count", SortOrder.DESC);
                break;
            }
            case -2:{
                sourceBuilder.sort("year", SortOrder.ASC);
                break;
            }
            case -3:{
                sourceBuilder.sort("n_citation", SortOrder.ASC);
                break;
            }
            case -4:{
                sourceBuilder.sort("vis_count", SortOrder.ASC);
                break;
            }
        }
        return sourceBuilder;
    }


}
