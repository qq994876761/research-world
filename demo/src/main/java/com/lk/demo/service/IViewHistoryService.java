package com.lk.demo.service;

public interface IViewHistoryService {
    Boolean viewHistory(Integer userId,String paperId);
} 