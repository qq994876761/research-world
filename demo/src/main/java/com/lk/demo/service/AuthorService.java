package com.lk.demo.service;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import com.lk.demo.common.R;
import com.lk.demo.entity.wrapper.MultiSearchCondition;
import com.lk.demo.entity.wrapper.Condition;
import com.lk.demo.entity.wrapper.SimpleSearchCondition;
import org.elasticsearch.ElasticsearchException;
import org.elasticsearch.ElasticsearchStatusException;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.get.MultiGetItemResponse;
import org.elasticsearch.action.get.MultiGetRequest;
import org.elasticsearch.action.get.MultiGetResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.support.WriteRequest;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.text.Text;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.*;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightField;
import org.elasticsearch.search.suggest.Suggest;
import org.elasticsearch.search.suggest.SuggestBuilder;
import org.elasticsearch.search.suggest.completion.CompletionSuggestion;
import org.elasticsearch.search.suggest.completion.CompletionSuggestionBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.concurrent.TimeUnit;

@Service
@EnableCaching
public class AuthorService {
    private final RestHighLevelClient esClient;
    private final PaperService paperService;
    private final ScholarService scholarService;

    @Autowired
    public AuthorService(RestHighLevelClient restHighLevelClient, PaperService paperService, ScholarService scholarService){
        this.esClient = restHighLevelClient;
        this.paperService = paperService;
        this.scholarService = scholarService;
    }

    public JSONArray getAuthorByID(String ID) {
        if(ID==null||ID.equals(""))
            return null;

        SearchRequest sr = new SearchRequest();
        sr.indices("author");

        TermQueryBuilder term = QueryBuilders.termQuery("id", ID);
        QueryBuilder query = QueryBuilders.boolQuery().must(term);

        SearchSourceBuilder ssb = new SearchSourceBuilder();
        ssb.query(query);
        ssb.from(0);
        ssb.size(1);
        ssb.timeout(new TimeValue(10, TimeUnit.SECONDS));
        sr.source(ssb);
        SearchResponse result = null;
        try {
            result = esClient.search(sr, RequestOptions.DEFAULT);
        } catch (ElasticsearchStatusException e) {
            if (e.status().name().equals("NOT_FOUND")) {
                return new JSONArray();
            }
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

        JSONArray jsonArray = new JSONArray();
        for (SearchHit hit : result.getHits().getHits()) {
            Map<String, Object> map = hit.getSourceAsMap();
            map.put("score", hit.getScore());
            jsonArray.add(new JSONObject(map));
        }
        return jsonArray;
    }

    public JSONArray getAuthorByIDs(ArrayList<String> IDs) {
        MultiGetRequest mRequest = new MultiGetRequest();
        for(String ID:IDs) {
            mRequest.add(new MultiGetRequest.Item("author", ID));
        }
        if(mRequest.getItems().isEmpty())
            return new JSONArray();
        MultiGetResponse mResponse = null;
        try{
            mResponse = esClient.mget(mRequest,RequestOptions.DEFAULT);
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }
        if(mResponse == null)
            return null;

        JSONArray result = new JSONArray();
        for(MultiGetItemResponse item: mResponse.getResponses()) {
            if (item.getFailure() != null) {
                Exception e = item.getFailure().getFailure();
                ElasticsearchException ee = (ElasticsearchException) e;
                JSONObject obj = new JSONObject();
                obj.put(item.getId(), ee.getMessage());
                result.add(new JSONObject(obj));
                continue;
            }
            GetResponse getResponse = item.getResponse();
            if (getResponse.isExists()) {
                JSONObject author = new JSONObject(getResponse.getSourceAsMap());
                JSONObject obj = new JSONObject();
                obj.put(item.getId(), author);
                result.add(new JSONObject(obj));
            }
            else {
                JSONObject obj = new JSONObject();
                obj.put(item.getId(), "ID not exist");
                result.add(obj);
            }
        }
        return result;
    }

    public JSONObject getAuthorObject(String id){
        JSONArray ja = getAuthorByID(id);
        if(ja==null||ja.isEmpty())
            return null;
        return ja.getJSONObject(0);
    }

    public JSONArray authorSimpleSearchService(SimpleSearchCondition cond) {
        SearchRequest sr = new SearchRequest();
        sr.indices("author");
        if (cond.value == null || cond.value.equals(""))
            return null;

        QueryBuilder query = QueryBuilders.boolQuery().must(QueryBuilders.multiMatchQuery(cond.value, "name", "tags.t", "orgs").minimumShouldMatch("15%"));

        SearchSourceBuilder ssb = new SearchSourceBuilder();
        ssb.query(query);
        ssb.from(cond.page*cond.size);
        ssb.size(cond.size);
        ssb.timeout(new TimeValue(10, TimeUnit.SECONDS));

        HighlightBuilder highlightBuilder = new HighlightBuilder();
        //设置高亮字段
        highlightBuilder.field("tags.t").field("name").field("orgs");

        //如果要多个字段高亮,这项要为false
        highlightBuilder.requireFieldMatch(false);
        highlightBuilder.preTags("<span style='color:red'>");
        highlightBuilder.postTags("</span>");

        highlightBuilder.fragmentSize(800000); //最大高亮分片数
        highlightBuilder.numOfFragments(0); //从第一个分片获取高亮片段
        ssb.highlighter(highlightBuilder);

        sr.source(ssb);
        SearchResponse result = null;

        try {
            result = esClient.search(sr, RequestOptions.DEFAULT);
        } catch (ElasticsearchStatusException e) {
            if (e.status().name().equals("NOT_FOUND")) {
                return new JSONArray();
            }
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

        JSONArray jsonArray = new JSONArray();
        for (SearchHit hit : result.getHits().getHits()) {
            Map<String, Object> map = hit.getSourceAsMap();
            map.put("score", hit.getScore());
            Map<String, HighlightField> highlightFields = hit.getHighlightFields();
            for(String s :new String[]{"name","tags.t","orgs"}){
                String light = getHighLightString(highlightFields,s);
                if(!light.equals(""))
                    map.put(s,light);
            }
            jsonArray.add(new JSONObject(map));
        }
        return jsonArray;
    }

    String getHighLightString(Map<String, HighlightField> highlightFields,String fieldName){
        HighlightField field= highlightFields.get(fieldName);
        if(field!= null){
            Text[] fragments = field.fragments();
            StringBuilder n_field = new StringBuilder();
            for (Text fragment : fragments) {
                n_field.append(fragment);
            }
            return n_field.toString();
        }
        return "";
    }

    public JSONArray authorMultiSearchService(MultiSearchCondition cond) {
        SearchRequest sr = new SearchRequest();
        sr.indices("author");
        BoolQueryBuilder query = QueryBuilders.boolQuery();
        boolean must = false;
        boolean should = false;
        boolean must_not = false;

        HashSet<String>highlightSet = new HashSet<>();
        HighlightBuilder highlightBuilder = new HighlightBuilder();

        highlightBuilder.requireFieldMatch(false);
        highlightBuilder.preTags("<span style='color:red'>");
        highlightBuilder.postTags("</span>");

        highlightBuilder.fragmentSize(800000); //最大高亮分片数
        highlightBuilder.numOfFragments(0); //从第一个分片获取高亮片段

        if (!cond.must.isEmpty()) {
            for (Condition c : cond.must) {
                if (c.key.equals("") || c.value.equals(""))
                    continue;
                if (c.key.equals("tags"))
                    c.key = "tags.t";
                if (c.key.equals("pubs"))
                    c.key = "pubs.i";
                if(!highlightSet.contains(c.key)) {
                    highlightBuilder.field(c.key);
                    highlightSet.add(c.key);
                }
                query.must(QueryBuilders.matchQuery(c.value, c.key));
                must = true;
            }
        }
        if (!cond.should.isEmpty()) {
            for (Condition c : cond.should) {
                if (c.key.equals("") || c.value.equals(""))
                    continue;
                if (c.key.equals("tags"))
                    c.key = "tags.t";
                if (c.key.equals("pubs"))
                    c.key = "pubs.i";
                if(!highlightSet.contains(c.key)) {
                    highlightBuilder.field(c.key);
                    highlightSet.add(c.key);
                }
                query.should(QueryBuilders.matchQuery(c.value, c.key));
                should = true;
            }
        }
        if (!cond.must_not.isEmpty()) {
            for (Condition c : cond.must_not) {
                if (c.key.equals("") || c.value.equals(""))
                    continue;
                if (c.key.equals("tags"))
                    c.key = "tags.t";
                if (c.key.equals("pubs"))
                    c.key = "pubs.i";
                if(!highlightSet.contains(c.key)) {
                    highlightBuilder.field(c.key);
                    highlightSet.add(c.key);
                }
                query.mustNot(QueryBuilders.matchQuery(c.value, c.key));
                must_not = true;
            }
        }
        if(!(must||must_not||should))
            return null;
        SearchSourceBuilder ssb = new SearchSourceBuilder();
        ssb.query(query);
        ssb.from(cond.page*cond.size);
        ssb.size(cond.size);
        ssb.timeout(new TimeValue(10, TimeUnit.SECONDS));
        ssb.highlighter(highlightBuilder);
        sr.source(ssb);
        SearchResponse result = null;
        try {
            result = esClient.search(sr, RequestOptions.DEFAULT);
        } catch (ElasticsearchStatusException e) {
            if (e.status().name().equals("NOT_FOUND")) {
                return new JSONArray();
            }
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

        JSONArray jsonArray = new JSONArray();
        for (SearchHit hit : result.getHits().getHits()) {
            Map<String, Object> map = hit.getSourceAsMap();
            map.put("score", hit.getScore());
            Map<String, HighlightField> highlightFields = hit.getHighlightFields();
            for(String s :highlightSet){
                String light = getHighLightString(highlightFields,s);
                if(!light.equals(""))
                    map.put(s,light);
            }
            jsonArray.add(new JSONObject(map));
        }
        return jsonArray;
    }

    public JSONArray getAuthorRelationService(String ID) {
        JSONObject author = getAuthorObject(ID);
        if (author == null)
            return null;
        JSONArray relation = author.getJSONArray("relation");

        if (relation != null && relation.size()!=0) {
            System.out.println("ok");
           return relation;
        }

        JSONArray pubs = author.getJSONArray("pubs");
        if (pubs == null || pubs.isEmpty())
            return new JSONArray();

        HashMap<String, JSONObject> authorMap = new HashMap<>();

        MultiGetRequest paperRequest = new MultiGetRequest();
        for (int i = 0; i < pubs.size(); ++i) {
            JSONObject obj = pubs.getJSONObject(i);
            if (!obj.containsKey("i"))
                continue;
            paperRequest.add(new MultiGetRequest.Item("paper", obj.getString("i")));
        }

        if(paperRequest.getItems().isEmpty())
            return new JSONArray();
        MultiGetResponse paperResponse = null;
        try{
            paperResponse = esClient.mget(paperRequest,RequestOptions.DEFAULT);
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }
        if(paperResponse == null)
            return null;

        MultiGetRequest authorRequest = new MultiGetRequest();
        for(MultiGetItemResponse item: paperResponse.getResponses()) {
            if (item.getFailure() != null) {
                Exception e = item.getFailure().getFailure();
                ElasticsearchException ee = (ElasticsearchException) e;
                if (ee.getMessage().contains("reason=no such index"))
                    continue;
            }
            GetResponse getResponse = item.getResponse();
            if (getResponse.isExists()) {
                JSONObject paper = new JSONObject(getResponse.getSourceAsMap());
                JSONArray authors = paper.getJSONArray("authors");
                if (authors == null || authors.isEmpty())
                    continue;
                for (int j = 0; j < authors.size(); ++j) {
                    JSONObject paperAuthor = authors.getJSONObject(j);
                    if (!paperAuthor.containsKey("id"))
                        continue;
                    if (paperAuthor.getString("id").equals(ID))
                        continue;
                    if (authorMap.containsKey(paperAuthor.getString("id"))) {
                        JSONObject elem = authorMap.get(paperAuthor.getString("id"));
                        int num = elem.getInteger("times") + 1;
                        elem.put("times", num);
                    } else {
                        com.alibaba.fastjson.JSONObject elem = new com.alibaba.fastjson.JSONObject();
                        elem.put("id", paperAuthor.getString("id"));
                        elem.put("name", paperAuthor.getString("name"));
                        elem.put("times", 1);
                        authorMap.put(paperAuthor.getString("id"), elem);
                        authorRequest.add(new MultiGetRequest.Item("author", paperAuthor.getString("id")));
                    }
                }
            }
        }

        MultiGetResponse authorResponse = null;
        if(authorRequest.getItems().isEmpty()){
            return new JSONArray();
        }
        try{
            authorResponse = esClient.mget(authorRequest,RequestOptions.DEFAULT);
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }
        if(authorResponse == null)
            return null;

        for(MultiGetItemResponse auItem: authorResponse.getResponses()) {
            if (auItem.getFailure() != null) {
                Exception e = auItem.getFailure().getFailure();
                ElasticsearchException ee = (ElasticsearchException) e;
                if (ee.getMessage().contains("reason=no such index"))
                    continue;
            }
            GetResponse auResponse = auItem.getResponse();
            if (!auResponse.isExists()) {
                authorMap.remove(auResponse.getId());
            }else{
                if(authorMap.containsKey(auResponse.getId())) {
                    JSONObject elem = authorMap.get(auResponse.getId());
                    JSONObject jo = new JSONObject(auResponse.getSourceAsMap());
                    elem.put("name", jo.getString("name"));
                    if(jo.containsKey("n_citation")){
                        elem.put("n_citation", jo.getString("n_citation"));
                    }
                    if(jo.containsKey("orgs")){
                        JSONArray orgs = jo.getJSONArray("orgs");
                        if(orgs.size()>0){
                            elem.put("affi",orgs.get(0));
                        }
                    }
                    authorMap.put(auResponse.getId(),elem);
                }
            }
        }

        relation = new JSONArray();
        if(!authorMap.isEmpty()){
            relation.addAll(authorMap.values());
            updateRelation(ID,relation);
        }
        return relation;
    }

    @CacheEvict(value="EsCache",key="'AuthorID'+#id")
    public void updateRelation(String id,JSONArray relation){
        JSONObject obj = new JSONObject();
        obj.put("relation",relation);

        UpdateRequest updateRequest = new UpdateRequest("author", id);

        updateRequest.doc(obj.toJSONString(),XContentType.JSON);
        updateRequest.timeout(TimeValue.timeValueSeconds(1));
        updateRequest.setRefreshPolicy(WriteRequest.RefreshPolicy.WAIT_UNTIL);
        try {
            esClient.update(updateRequest, RequestOptions.DEFAULT);
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public JSONArray suggest(String prefix,String field,int size){
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        CompletionSuggestionBuilder suggestionBuilderDistrict =
                new CompletionSuggestionBuilder(field+".suggest").prefix(prefix).size(size);
        SuggestBuilder suggestBuilder = new SuggestBuilder();
        suggestBuilder.addSuggestion("my_suggest", suggestionBuilderDistrict);
        searchSourceBuilder.suggest(suggestBuilder);
        SearchRequest searchRequest = new SearchRequest();
        searchRequest.indices("author");
        searchRequest.source(searchSourceBuilder);
        SearchResponse response = null;
        try {
            response = esClient.search(searchRequest, RequestOptions.DEFAULT);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        HashSet<String>suggestSet = new HashSet<>();
        int cnt =0;
        Suggest suggest = response.getSuggest();
        if(suggest != null){
            for (Object term : suggest.getSuggestion("my_suggest").getEntries()) {
                if (term instanceof CompletionSuggestion.Entry) {
                    CompletionSuggestion.Entry item = (CompletionSuggestion.Entry) term;
                    if (!item.getOptions().isEmpty()) {
                        for (CompletionSuggestion.Entry.Option option : item.getOptions()) {
                            String tip = option.getText().toString();
                            if (!suggestSet.contains(tip)) {
                                suggestSet.add(tip);
                                cnt++;
                            }
                        }
                    }
                }
                if (cnt >= size) {
                    break;
                }
            }
        }
        JSONArray ans = new JSONArray();
        ans.addAll(suggestSet);
        return ans;
    }

}
