package com.lk.demo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lk.demo.entity.Collection;

public interface CollectionMapper extends BaseMapper<Collection> {
}
