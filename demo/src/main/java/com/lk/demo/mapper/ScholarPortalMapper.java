package com.lk.demo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lk.demo.entity.ScholarPortal;

public interface ScholarPortalMapper extends BaseMapper<ScholarPortal> {
}
