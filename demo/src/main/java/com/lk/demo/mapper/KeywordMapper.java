package com.lk.demo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lk.demo.entity.Keyword;

public interface KeywordMapper extends BaseMapper<Keyword> {
}
