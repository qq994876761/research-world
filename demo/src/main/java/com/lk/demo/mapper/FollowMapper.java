package com.lk.demo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lk.demo.entity.Follow;

public interface FollowMapper extends BaseMapper<Follow> {
}
