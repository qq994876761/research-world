package com.lk.demo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lk.demo.entity.Scholar;

import java.util.ArrayList;

public interface ScholarMapper extends BaseMapper<Scholar> {
    Scholar selectByAuthorId(String authorId);
    ArrayList<Scholar> selectByPaperId(String paperId);
}
