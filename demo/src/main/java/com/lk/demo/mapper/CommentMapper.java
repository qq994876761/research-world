package com.lk.demo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lk.demo.entity.Comment;

public interface CommentMapper extends BaseMapper<Comment> {
}
