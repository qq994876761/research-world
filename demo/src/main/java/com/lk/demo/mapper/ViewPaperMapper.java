package com.lk.demo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lk.demo.entity.ViewPaper;

import java.util.ArrayList;

public interface ViewPaperMapper extends BaseMapper<ViewPaper> {
    ArrayList<String> selectHot();
    ArrayList<String> selectHotByField(String field);
}
