package com.lk.demo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lk.demo.entity.ViewField;

import java.util.List;

public interface ViewFieldMapper extends BaseMapper<ViewField> {
    List<String> selectHot();
}
