package com.lk.demo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lk.demo.entity.ViewHistory;

public interface ViewHistoryMapper extends BaseMapper<ViewHistory> {
}
