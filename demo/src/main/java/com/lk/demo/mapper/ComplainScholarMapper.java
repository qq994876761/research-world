package com.lk.demo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lk.demo.entity.ComplainScholar;

import java.util.List;

public interface ComplainScholarMapper extends BaseMapper<ComplainScholar> {
    public List getalluser();
    public List getallauthor();
    public List getall();
}
