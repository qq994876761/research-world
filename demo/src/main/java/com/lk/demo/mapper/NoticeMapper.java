package com.lk.demo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lk.demo.entity.Notice;

public interface NoticeMapper extends BaseMapper<Notice> {
}
