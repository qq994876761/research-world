package com.lk.demo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lk.demo.entity.ComplainPaper;

import java.util.List;

public interface ComplainPaperMapper extends BaseMapper<ComplainPaper> {
    public List getall();
}
