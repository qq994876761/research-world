package com.lk.demo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lk.demo.entity.Like;

public interface LikeMapper extends BaseMapper<Like> {
}
