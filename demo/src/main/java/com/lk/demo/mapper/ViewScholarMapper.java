package com.lk.demo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lk.demo.entity.ViewScholar;

import java.util.List;

public interface ViewScholarMapper extends BaseMapper<ViewScholar> {
    List<Integer> selectHot();
}
