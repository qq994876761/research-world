package com.lk.demo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lk.demo.entity.Affiliation;

public interface AffiliationMapper extends BaseMapper<Affiliation> {
}
