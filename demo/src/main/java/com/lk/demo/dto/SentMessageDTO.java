package com.lk.demo.dto;

import com.lk.demo.entity.Message;
import com.lk.demo.entity.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class SentMessageDTO implements Comparable<SentMessageDTO>{
    private Message message;
    private User owner;

    @Override
    public int compareTo(SentMessageDTO o) {
        return o.message.getCreateTime().compareTo(this.message.getCreateTime());
    }
}
