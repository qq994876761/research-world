package com.lk.demo.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class FollowQuery {
    private Integer followerId;
    private ArrayList<Integer> followingIds;
}
