package com.lk.demo.dto;

import com.alibaba.fastjson.JSONObject;
import com.lk.demo.entity.ViewHistory;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class ViewHistoryDTO {
    private ViewHistory viewHistory;
    private JSONObject paper;
}
