package com.lk.demo.dto;

import com.lk.demo.entity.Comment;
import com.lk.demo.entity.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class CommentDTO {
    private Comment comment;
    private User commenter;
    private Boolean isLike;
}