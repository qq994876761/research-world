package com.lk.demo.dto;

import com.lk.demo.entity.Message;
import com.lk.demo.entity.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class MessageDTO implements Comparable<MessageDTO>{
    private Message message;
    private User sender;

    @Override
    public int compareTo(MessageDTO o) {
        return o.message.getCreateTime().compareTo(this.message.getCreateTime());
    }
}
