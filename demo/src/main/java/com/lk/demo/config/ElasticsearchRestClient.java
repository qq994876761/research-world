package com.lk.demo.config;

import org.apache.http.HttpHost;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestClientBuilder;
import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;

@Configuration
public class ElasticsearchRestClient {

    @Value("${elasticsearch.ip}")
    String ipPort;

    @Bean(name = "restHighLevelClient")
    public RestHighLevelClient highLevelClient(@Autowired RestClientBuilder restClientBuilder) {
        //restClientBuilder.setMaxRetryTimeoutMillis(60000);
        return new RestHighLevelClient(RestClient.builder(makeHttpHost(ipPort)));
    }


    private HttpHost[] makeHttpHost(String s) {
        ArrayList<HttpHost> httpHosts = new ArrayList<>();
        try{
            String[] ipPorts = s.split(",");
            for(String ipPort: ipPorts) {
                String[] address = ipPort.split(":");
                String ip = new String(address[0]);
                int port = Integer.parseInt(address[1]);
                httpHosts.add(new HttpHost(ip, port, "http"));
            }
            if(httpHosts.isEmpty()){
                httpHosts.add(new HttpHost("localhost", 9200, "http"));
            }
            HttpHost[] hosts = new HttpHost[httpHosts.size()];
            return httpHosts.toArray(hosts);
        }catch(Exception e){
            System.out.println("ES IP incorrect");
            e.printStackTrace();
            if(httpHosts.isEmpty()){
                httpHosts.add(new HttpHost("localhost", 9200, "http"));
            }
            HttpHost[] hosts = new HttpHost[httpHosts.size()];
            return httpHosts.toArray(hosts);
        }
    }
}
