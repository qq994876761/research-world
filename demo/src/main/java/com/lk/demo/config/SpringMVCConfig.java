package com.lk.demo.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.io.File;

@Configuration
public class SpringMVCConfig implements WebMvcConfigurer {
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        //File file = new File("");
        //String path = file.getAbsolutePath().substring(0, file.getAbsolutePath().indexOf(File.separator));
        //System.out.println(path);
        //判断操作系统
        String os = System.getProperty("os.name");
        if (os.toLowerCase().startsWith("win")) {  //如果是Windows系统
            //项目相对路径+项目动态绝对路径
            registry.addResourceHandler("/avatar/**").
                    addResourceLocations("file:" + "F:/avatar/");
            registry.addResourceHandler("/complain/**").
                    addResourceLocations("file:" + "F:/complain/");
        } else {//linux和mac系统
            registry.addResourceHandler("/avatar/**").
                    addResourceLocations("file:" + "/mnt/sdc/springboot/avatar/");
            registry.addResourceHandler("/complain/**").
                    addResourceLocations("file:" + "/mnt/sdc/springboot/complain/");
        }
    }


}