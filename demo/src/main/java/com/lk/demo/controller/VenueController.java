package com.lk.demo.controller;


import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.lk.demo.common.R;
import com.lk.demo.entity.wrapper.paper.ReturnPapers;
import com.lk.demo.entity.wrapper.venue.VenueName;
import com.lk.demo.service.DataService;
import com.lk.demo.service.VenueService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/venue")
@Api(description = "期刊/会议有关接口")
public class VenueController {
    private final VenueService venueService;
    private final DataService dataService;

    public VenueController(VenueService venueService, DataService dataService) {
        this.venueService = venueService;
        this.dataService = dataService;
    }
    /*已测试*/
    @PostMapping("/get-venue")
    @ApiOperation(notes = "基本用不上", value = "根据ID查找期刊/会议")
    public R getVenueByID(@RequestParam("Id") String Id){
        JSONObject object = venueService.geVenueByID(Id);
        if(object==null){
            return R.fail("not found");
        }
//        System.out.println(object);
        return R.success(object);
    }
    /*已测试*/
    @GetMapping("/get-all-venue")
    @ApiOperation(notes = "用于最开始的期刊/会议展示页面", value = "获取所有期刊/会议信息")
    public R getAllVenue(@RequestParam("size") Integer size,@RequestParam("page") Integer page){
        JSONArray object = venueService.getAllVenue(size,page);
        if(object==null){
            return R.fail("not found");
        }
//        System.out.println(object);
        return R.success(object);
    }


    /*已测试*/
    @PostMapping("/get-venue-by-name")
    @ApiOperation(notes = "常规的page和size", value = "根据名字来搜索期刊/会议")
    public R getVenueByname(@RequestBody VenueName venueName){
        ReturnPapers object = venueService.getVenueByname(venueName);
        if(object==null){
            return R.fail("not found");
        }
//        System.out.println(object);
        return R.success(object);
    }
    /**
     * TODO
     * 期刊/会议统计信息欲计算。
     *
     * **/
}
