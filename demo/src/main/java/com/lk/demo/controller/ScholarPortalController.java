package com.lk.demo.controller;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.lk.demo.common.R;
import com.lk.demo.entity.Scholar;
import com.lk.demo.entity.ScholarPortal;
import com.lk.demo.entity.User;
import com.lk.demo.mapper.ScholarMapper;
import com.lk.demo.mapper.ScholarPortalMapper;
import com.lk.demo.service.PaperService;
import com.lk.demo.service.impl.HotIndexServiceImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import java.awt.print.Paper;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/scholarPortal")
@Api(description = "学者门户相关接口")
public class ScholarPortalController {
    @Resource
    private ScholarMapper scholarMapper;
    @Resource
    private ScholarPortalMapper scholarPortalMapper;
    @Resource
    private PaperService paperService;
    @Resource
    private HotIndexServiceImpl hotIndexService;


    @GetMapping("")
    @ApiOperation(notes = "无说明", value = "获取学者门户")
    public R get(@RequestParam("scholarId") Integer scholarId) {
//        HashMap<String,Object> columnMap = new HashMap<>() ;
//        ScholarPortal scholarPortal=scholarPortalMapper.selectByMap(columnMap);
        QueryWrapper<ScholarPortal> wrapper = new QueryWrapper<>();
        wrapper.eq("scholar_id", scholarId);
        ScholarPortal scholarPortal = scholarPortalMapper.selectOne(wrapper);
        if (null == scholarPortal)
            return R.fail("学者id不存在门户");
        hotIndexService.viewScholar(scholarId);
        return R.success(scholarPortal);
    }

    @PutMapping("")
    @ApiOperation(notes = "无说明", value = "编辑学者门户")
    public R update(@RequestBody ScholarPortal scholarPortal) {
        if (scholarPortalMapper.updateById(scholarPortal) == 0)
            return R.fail("学者id不存在门户");
        return R.success();
    }

    /*已测试*/
    @PostMapping("/getpapershow")
    @ApiOperation(notes = "无参数,返回值为list<paper>", value = "获得paper展示列表")
    public R getpapershow(@RequestBody JSONObject json, HttpSession session) {
        try {
            if (session.getAttribute("loginUser") == null) {
                return R.fail("此操作需要先登录");
            }
            User user = (User) session.getAttribute("loginUser");

            QueryWrapper<Scholar> wrapper = new QueryWrapper<>();
            wrapper.eq("user_id", user.getId());
            Scholar scholar = scholarMapper.selectOne(wrapper);
            if (scholar == null) {
                return R.fail("您还不是学者");
            }

            QueryWrapper<ScholarPortal> wrapper1 = new QueryWrapper<>();
            wrapper1.eq("scholar_id", scholar.getId());

            ScholarPortal scholarPortal = scholarPortalMapper.selectOne(wrapper1);

            if(scholarPortal.Getpaper()==null || scholarPortal.getPaperShow().equals("")) return R.success(null);
            else{
                System.out.println((ArrayList<String>) scholarPortal.Getpaper());
                return R.success(paperService.getPaperByIDs((ArrayList<String>) scholarPortal.Getpaper()));
            }


        } catch (Exception e) {
            e.printStackTrace();
            return R.fail("未知错误");
        }
    }

    @PostMapping("/getpapershow-byscholarId")
    @ApiOperation(notes = "无需登录,需参数scholarId,返回值为list<paper>", value = "根据学者id获得他人paper展示列表")
    public R getpapershowbyscholarId(@RequestBody JSONObject json, HttpSession session) {
        try {
            if (json.get("scholarId") == null) {
                return R.fail("无参数");
            }

            QueryWrapper<Scholar> wrapper = new QueryWrapper<>();
            wrapper.eq("id",json.get("scholarId"));
            Scholar scholar = scholarMapper.selectOne(wrapper);
            if (scholar == null) {
                return R.fail("找不到该学者");
            }

            QueryWrapper<ScholarPortal> wrapper1 = new QueryWrapper<>();
            wrapper1.eq("scholar_id", scholar.getId());

            ScholarPortal scholarPortal = scholarPortalMapper.selectOne(wrapper1);
            System.out.println(scholarPortal);
            System.out.println(scholarPortal.Getpaper());
            if(scholarPortal.Getpaper()==null || scholarPortal.getPaperShow().equals("")) return R.success(null);
            else{
                System.out.println((ArrayList<String>) scholarPortal.Getpaper());
                return R.success(paperService.getPaperByIDs((ArrayList<String>) scholarPortal.Getpaper()));
            }


        } catch (Exception e) {
            e.printStackTrace();
            return R.fail("未知错误");
        }
    }

    /*已测试*/
    @PostMapping("/addpaper")
    @ApiOperation(notes = "参数为paperid列表：ids", value = "批量添加paper到展示列表")
    public R addpaper(@RequestBody JSONObject json, HttpSession session) {
        try {
            if (json.get("ids") == null) {
                return R.fail("没有参数ids？");
                //scholarid=(Integer) json.get("scholarid");
            }
//            List<String> ids = new ArrayList<>(Arrays.asList((String[]) json.get("ids")));
            List<String> ids = new ArrayList<>((ArrayList<String>) json.get("ids"));
            if (session.getAttribute("loginUser") == null) {
                return R.fail("此操作需要先登录");
            }
            User user = (User) session.getAttribute("loginUser");

            QueryWrapper<Scholar> wrapper = new QueryWrapper<>();
            wrapper.eq("user_id", user.getId());
            Scholar scholar = scholarMapper.selectOne(wrapper);
            if (scholar == null) {
                return R.fail("您还不是学者");
            }

            QueryWrapper<ScholarPortal> wrapper1 = new QueryWrapper<>();
            wrapper1.eq("scholar_id", scholar.getId());
            ScholarPortal scholarPortal = scholarPortalMapper.selectOne(wrapper1);

            for (String id : ids) {
                scholarPortal.addpaper(id);
            }
            scholarPortalMapper.update(scholarPortal, wrapper1);

            return R.success(scholarPortal);

        } catch (Exception e) {
            e.printStackTrace();
            return R.fail("未知错误");
        }
    }

    /*已测试*/
    @PostMapping("/deletepaper")
    @ApiOperation(notes = "参数为paperid列表：ids", value = "批量删除展示列表中的paper")
    public R deletepaper(@RequestBody JSONObject json, HttpSession session) {
        try {
            if (json.get("ids") == null) {
                return R.fail("没有参数ids？");
                //scholarid=(Integer) json.get("scholarid");
            }
            List<String> ids = new ArrayList<>((ArrayList<String>) json.get("ids"));
            if (session.getAttribute("loginUser") == null) {
                return R.fail("此操作需要先登录");
            }
            User user = (User) session.getAttribute("loginUser");

            QueryWrapper<Scholar> wrapper = new QueryWrapper<>();
            wrapper.eq("user_id", user.getId());
            Scholar scholar = scholarMapper.selectOne(wrapper);
            if (scholar == null) {
                return R.fail("您还不是学者");
            }

            QueryWrapper<ScholarPortal> wrapper1 = new QueryWrapper<>();
            wrapper1.eq("scholar_id", scholar.getId());
            ScholarPortal scholarPortal = scholarPortalMapper.selectOne(wrapper1);

            for (String id : ids) {
                scholarPortal.deletepaper(id);
            }
            scholarPortalMapper.update(scholarPortal, wrapper1);

            return R.success(scholarPortal);

        } catch (Exception e) {
            e.printStackTrace();
            return R.fail("未知错误");
        }
    }



}
