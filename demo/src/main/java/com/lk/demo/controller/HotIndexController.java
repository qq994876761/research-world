package com.lk.demo.controller;

import com.alibaba.fastjson.JSONObject;
import com.lk.demo.common.R;
import com.lk.demo.entity.Scholar;
import com.lk.demo.mapper.ScholarMapper;
import com.lk.demo.mapper.ViewFieldMapper;
import com.lk.demo.mapper.ViewPaperMapper;
import com.lk.demo.mapper.ViewScholarMapper;
import com.lk.demo.service.PaperService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@RestController
@RequestMapping("/hotIndex")
@Api(description = "热度相关接口")
public class HotIndexController {

    @Resource
    private ViewFieldMapper viewFieldMapper;
    @Resource
    private ViewPaperMapper viewPaperMapper;
    @Resource
    private ViewScholarMapper viewScholarMapper;
    @Resource
    private ScholarMapper scholarMapper;
    @Resource
    private PaperService paperService;

    @GetMapping("")
    @ApiOperation(notes = "无说明", value = "获取热点列表")
    @Cacheable(value = "MysqlCache", key = "'hotIndex'")
    public R get() {
        HashMap<String, Object> hotIndexMessages = new HashMap<>();
        List<String> fieldIds = viewFieldMapper.selectHot();
        ArrayList<String> paperIds = viewPaperMapper.selectHot();
        List<Integer> scholarIds = viewScholarMapper.selectHot();
        hotIndexMessages.put("fields", fieldIds);
        hotIndexMessages.put("papers", paperService.getPaperByIDs(paperIds));
        List<Scholar> scholars = new ArrayList<>();
        for (Integer scholarId : scholarIds)
            scholars.add(scholarMapper.selectById(scholarId));
        hotIndexMessages.put("scholars", scholars);
        return R.success(hotIndexMessages);
    }

    @GetMapping("/field")
    @ApiOperation(notes = "无说明", value = "获取指定领域热点列表")
    @Cacheable(value = "MysqlCache", key = "'hotIndex'+#field")
    public R getByField(@RequestParam("field") String field) {
        ArrayList<String> paperIds;
        if (field == null || field.equals("all"))
            paperIds = viewPaperMapper.selectHot();
        else
            paperIds = viewPaperMapper.selectHotByField(field);
        List<JSONObject> papers = new ArrayList<>();
        for (String paperId : paperIds)
            papers.add(paperService.getPaperByID(paperId));
        return R.success(papers);
    }

//    @PostMapping("viewField")
//    @ApiOperation(notes = "无说明", value = "为领域增加热度")
//    public R viewField(@RequestBody ViewField viewField) {
//        viewFieldMapper.insert(viewField);
//        return R.success();
//    }
//
//    @PostMapping("viewPaper")
//    @ApiOperation(notes = "无说明", value = "为学术成果增加热度")
//    public R viewPaper(@RequestBody ViewPaper viewPaper) {
//        viewPaperMapper.insert(viewPaper);
//        paperService.visit(viewPaper.getPaperId());
//        return R.success();
//    }
//
//    @PostMapping("viewScholar")
//    @ApiOperation(notes = "无说明", value = "为学者增加热度")
//    public R viewScholar(@RequestBody ViewScholar viewScholar) {
//        viewScholarMapper.insert(viewScholar);
//        return R.success();
//    }
}
