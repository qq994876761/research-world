package com.lk.demo.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.lk.demo.common.R;
import com.lk.demo.dto.CommentDTO;
import com.lk.demo.dto.MessageDTO;
import com.lk.demo.dto.SentMessageDTO;
import com.lk.demo.entity.*;
import com.lk.demo.mapper.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

@RestController
@RequestMapping("/communication")
@Api(description = "信息相关接口")
public class CommunicationController {
    @Resource
    private CommentMapper commentMapper;
    @Resource
    private MessageMapper messageMapper;
    @Resource
    private NoticeMapper noticeMapper;
    @Resource
    private UserMapper userMapper;
    @Resource
    private ScholarMapper scholarMapper;
    @Resource
    private LikeMapper likeMapper;

    @GetMapping("/comment")
    @ApiOperation(notes = "无说明", value = "获取学术成果评论列表")
    public R getComment(@RequestParam("paperId") String paperId, @RequestParam("userId") Integer userId) {
        QueryWrapper<Comment> wrapper = new QueryWrapper<>();
        wrapper.eq("paper_id", paperId);
        List<Comment> comments = commentMapper.selectList(wrapper);
        List<CommentDTO> commentDTOs = new ArrayList<>();
        for (Comment comment : comments) {
            Boolean isLike = false;
            QueryWrapper<Like> likeQueryWrapper = new QueryWrapper<>();
            likeQueryWrapper.eq("comment_id", comment.getId());
            likeQueryWrapper.eq("user_id", userId);
            if (likeMapper.selectList(likeQueryWrapper).size() != 0)
                isLike = true;
            commentDTOs.add(new CommentDTO(comment, userMapper.selectById(comment.getUserId()), isLike));
        }
        return R.success(commentDTOs);
    }

    @PostMapping("/comment")
    @ApiOperation(notes = "无说明", value = "评论学术成果")
    public R postComment(@RequestBody Comment comment) {
        commentMapper.insert(comment);
        String paperId = comment.getPaperId();
        if (paperId == null)
            return R.fail("paperId不能为空");
        ArrayList<Scholar> scholars = scholarMapper.selectByPaperId(paperId);
        for (Scholar scholar : scholars) {
            Notice notice = new Notice();
            notice.setUserId(scholar.getUserId());
            User user = userMapper.selectById(scholar.getUserId());
            if (user != null) {
                notice.setContent(user.getName() + "评论了您的论文");
                noticeMapper.insert(notice);
            }
        }
        return R.success();
    }

    @DeleteMapping("/comment")
    @ApiOperation(notes = "无说明", value = "删除评论")
    public R deleteComment(@RequestParam("commentId") Integer commentId) {
        if (commentMapper.deleteById(commentId) == 0)
            return R.fail("评论id不存在");
        return R.success();
    }

    @PutMapping("/viewMessage")
    @ApiOperation(notes = "无说明", value = "设置消息为已读")
    public R viewMessage(@RequestParam Integer messageId) {
        Message message = messageMapper.selectById(messageId);
        if (message == null)
            return R.fail("通知id不存在");
        if (message.getIsRead())
            return R.fail("已读");
        message.setIsRead(true);
        messageMapper.updateById(message);
        return R.success();
    }

    @PostMapping("/message")
    @ApiOperation(notes = "无说明", value = "发送消息")
    public R postMessage(@RequestBody Message message) {
        messageMapper.insert(message);
        return R.success();
    }

    @GetMapping("/message")
    @ApiOperation(notes = "无说明", value = "获取消息列表")
    public R getMessage(@RequestParam("userId") Integer userId) {
        HashMap<String, Object> returnMap = new HashMap<>();

        QueryWrapper<Message> readWrapper = new QueryWrapper<>();
        readWrapper.eq("owner_id", userId);
        readWrapper.eq("is_read", true);
        List<Message> readMessages = messageMapper.selectList(readWrapper);
        List<MessageDTO> readMessageDTOS = new ArrayList<>();
        for (Message message : readMessages)
            readMessageDTOS.add(new MessageDTO(message, userMapper.selectById(message.getSenderId())));
        Collections.sort(readMessageDTOS);
        returnMap.put("readMessages", readMessageDTOS);

        QueryWrapper<Message> unreadWrapper = new QueryWrapper<>();
        unreadWrapper.eq("owner_id", userId);
        unreadWrapper.eq("is_read", false);
        List<Message> unreadMessages = messageMapper.selectList(unreadWrapper);
        List<MessageDTO> unreadMessageDTOS = new ArrayList<>();
        for (Message message : unreadMessages)
            unreadMessageDTOS.add(new MessageDTO(message, userMapper.selectById(message.getSenderId())));
        Collections.sort(unreadMessageDTOS);
        returnMap.put("unreadMessages", unreadMessageDTOS);

        QueryWrapper<Message> sentWrapper = new QueryWrapper<>();
        sentWrapper.eq("sender_id", userId);
        List<Message> sentMessages = messageMapper.selectList(sentWrapper);
        List<SentMessageDTO> sentMessageDTOS = new ArrayList<>();
        for (Message message : sentMessages)
            sentMessageDTOS.add(new SentMessageDTO(message, userMapper.selectById(message.getOwnerId())));
        Collections.sort(sentMessageDTOS);
        returnMap.put("sentMessages", sentMessageDTOS);

        return R.success(returnMap);
    }

    @DeleteMapping("/message")
    @ApiOperation(notes = "无说明", value = "删除消息")
    public R postMessage(@RequestParam(value="message") Integer messageId) {
        if(messageMapper.deleteById(messageId)==0)
            return R.fail("消息id不存在");
        return R.success();
    }

    @PutMapping("/viewNotice")
    @ApiOperation(notes = "无说明", value = "设置通知为已读")
    public R viewNotice(@RequestParam Integer noticeId) {
        Notice notice = noticeMapper.selectById(noticeId);
        if (notice == null)
            return R.fail("通知id不存在");
        if (notice.getIsRead())
            return R.fail("已读");
        notice.setIsRead(true);
        noticeMapper.updateById(notice);
        return R.success();
    }

    @PostMapping("/notice")
    @ApiOperation(notes = "无说明", value = "发送通知")
    public R postNotice(@RequestBody Notice notice) {
        noticeMapper.insert(notice);
        return R.success();
    }

    @GetMapping("/notice")
    @ApiOperation(notes = "无说明", value = "获取通知")
    public R getNotice(@RequestParam("userId") Integer userId) {
        QueryWrapper<Notice> wrapper = new QueryWrapper<>();
        wrapper.eq("user_id", userId);
        List<Notice> notices = noticeMapper.selectList(wrapper);
        Collections.sort(notices);
        return R.success(notices);
    }
}
