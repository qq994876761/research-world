package com.lk.demo.controller;


import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.lk.demo.common.R;
import com.lk.demo.entity.*;
import com.lk.demo.mapper.*;
import com.lk.demo.service.AuthorService;
import com.lk.demo.service.MailService;
import com.lk.demo.service.PaperService;
import com.lk.demo.service.ScholarService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.system.ApplicationHome;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.regex.Pattern;

import static com.lk.demo.common.VerCodeGenerateUtil.generateVerCode;

@RestController
@RequestMapping("/scholar")
@Api(description = "学者相关接口")
public class ScholarController {
    @Resource
    private ScholarMapper scholarMapper;
    @Resource
    private ScholarPortalMapper scholarPortalMapper;
    @Resource
    private UserMapper userMapper;
    @Autowired
    private MailService mailService;
    @Resource
    private PaperService paperService;
    @Resource
    private ScholarService scholarService;
    @Resource
    private AuthorService authorService;
    @Resource
    private NoticeMapper noticeMapper;
    @Resource
    private ViewScholarMapper viewScholarMapper;

    @GetMapping("/getByUserId")
    @ApiOperation(notes = "无说明", value = "获取学者信息")
    public R getByUserId(@RequestParam("userId") Integer userId) {
        QueryWrapper<Scholar> wrapper = new QueryWrapper<>();
        wrapper.eq("user_id", userId);
        Scholar scholar = scholarMapper.selectOne(wrapper);
        if (null == scholar)
            return R.fail("学者不存在");
        QueryWrapper<ViewScholar> viewScholarQueryWrapper = new QueryWrapper<>();
        viewScholarQueryWrapper.eq("scholar_id",scholar.getId());
        scholar.setHotIndex(viewScholarMapper.selectList(viewScholarQueryWrapper).size());
        return R.success(scholar);
    }

    @GetMapping("")
    @ApiOperation(notes = "无说明", value = "获取学者信息")
    public R get(@RequestParam("scholarId") Integer scholarId) {
//        HashMap<String,Object> columnMap = new HashMap<>() ;
//        ScholarPortal scholarPortal=scholarPortalMapper.selectByMap(columnMap);
        QueryWrapper<Scholar> wrapper = new QueryWrapper<>();
        wrapper.eq("id", scholarId);
        Scholar scholar = scholarMapper.selectOne(wrapper);
        if (null == scholar)
            return R.fail("学者id不存在");
        QueryWrapper<ViewScholar> viewScholarQueryWrapper = new QueryWrapper<>();
        viewScholarQueryWrapper.eq("scholar_id",scholar.getId());
        scholar.setHotIndex(viewScholarMapper.selectList(viewScholarQueryWrapper).size());
        return R.success(scholar);
    }

    @PutMapping("")
    @ApiOperation(notes = "无说明", value = "编辑学者信息")
    public R update(@RequestBody Scholar scholar) {
        if (scholarMapper.updateById(scholar) == 0)
            return R.fail("学者id不存在");
        return R.success();
    }

    @Resource
    private ComplainScholarMapper complainScholarMapper;

    /*已测试*/
    @PostMapping("/complain-scholar")
    @ApiOperation(notes = "需要被举报者的id(后端不验证id是否存在)、类型、具体理由,图片(可有可无)", value = "举报用户")
    public R complainScholar(@RequestParam(value = "complainedid", required=false) String complainedid,
                             @RequestParam(value = "type", required=false) Integer type,
                             @RequestParam(value = "reason", required=false) String reason,
                             @RequestParam(value = "paperId", required=false) String paperId,
                             @RequestParam(value = "file", required=false) MultipartFile file,
                             @RequestParam(value = "userId", required=false) Integer userId,
                             HttpSession session, HttpServletRequest request){
        try {
            if(session.getAttribute("loginUser")==null){
                return R.fail("此操作需要先登录");
            }
            User user=(User) session.getAttribute("loginUser");
            ComplainScholar complainScholar=new ComplainScholar();
            complainScholar.setComplainedid(complainedid);
            complainScholar.setType(type);
            complainScholar.setReason(reason);
            complainScholar.setPaperId(paperId);

            if(type==null || complainedid==null){
                return R.fail("缺少参数(type或complainedid)");
            }

            if(type/10==0){
                complainScholar.setScholarId(Integer.valueOf(complainedid));
            }else if(type/10==1){
                complainScholar.setScholarId(Integer.valueOf(complainedid));
                if(complainScholar.getPaperId()==null){
                    return R.fail("举报学者请附带paperid");
                }
            }else {
                complainScholar.setAuthorId(complainedid);

            }


            QueryWrapper<ComplainScholar> wrapper = new QueryWrapper<>();
            complainScholar.setStatus(0);  //未处理
            complainScholar.setUserId(user.getId());

            complainScholar.setCreateTime(new Date());
            //complainScholar.setId(11);
            complainScholarMapper.insert(complainScholar);
            //complainScholarMapper.updateById(complainScholar);
            if(file!=null){  //如果传了举报截图
                System.out.println("上传截图");
//                String dirPath = request.getServletContext().getRealPath("upload");//会在webapp下面创建此文件夹
//                System.out.println("dirPath=" + dirPath);
//
//                File dir = new File(dirPath);
//                if (!dir.exists()) {
//                    dir.mkdirs();
//                }
//                //2.确定保存的文件名
//                String orginalFilename = file.getOriginalFilename();
//                int beginIndex = orginalFilename.lastIndexOf(".");
//                String suffix = "";
//                if (beginIndex != -1) {
//                    suffix = orginalFilename.substring(beginIndex);
//                }
//                String filename = "举报用户截图"+complainScholar.getId().toString() + suffix;
//                //创建文件对象，表示要保存的头像文件,第一个参数表示存储的文件夹，第二个参数表示存储的文件
//                File dest = new File(dir, filename);
//                //执行保存
//                file.transferTo(dest);
//                String avatar = "/upload/" + filename;
//                complainScholar.setImage(avatar);
                String dirPath;
                String os = System.getProperty("os.name");
                if (os.toLowerCase().startsWith("win")) {  //如果是Windows系统
                    dirPath = "F:/complain/";
                } else {//linux和mac系统
                    ApplicationHome h = new ApplicationHome(getClass());
                    File jarF = h.getSource();
                    dirPath = jarF.getParentFile().toString()+"/complain/";
                }
                System.out.println("dirPath=" + dirPath);
                File dir = new File(dirPath);
                if (!dir.exists()) {
                    dir.mkdirs();
                }
                String orginalFilename = file.getOriginalFilename();
                int beginIndex = orginalFilename.lastIndexOf(".");
                String suffix = "";
                if (beginIndex != -1) {
                    suffix = orginalFilename.substring(beginIndex);
                }
                String filename = "举报用户截图"+complainScholar.getId().toString() + suffix;
                File dest = new File(dir, filename);
                file.transferTo(dest);
                String avatar = "/complain/" + filename;
                complainScholar.setImage(avatar);

            }


            Notice notice=new Notice();
            notice.setUserId(user.getId());
            notice.setIsRead(false);
            notice.setCreateTime(new Date());
            //notice.setId(1);
            String content="";
            SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            if(type/10==2){  //作者
                QueryWrapper<Scholar> wrapper0 = new QueryWrapper<>();
                wrapper0.eq("user_id",userId);
                Scholar scholar= scholarMapper.selectOne(wrapper0);
//                Integer scholarId=scholar.getId();
                complainScholar.setScholarId(scholar.getId());
                String name="";
                 if(scholar==null) name=complainedid+"(对应学者似乎不存在)";
                else name=scholar.getName();
                content+=ft.format(new Date()) +":\n\t您提交的对"+name+"的作者的举报已受理，我们会尽快核实处理，请您耐心等待，谢谢。";
            }else if(type/10==1) {
                Scholar scholar=scholarMapper.selectById(Integer.valueOf(complainedid));
                String name="";
                if(scholar==null) name=complainedid+"(学者似乎不存在)";
                else name=scholar.getName();

                JSONObject paper= paperService.getPaperByID(complainScholar.getPaperId());
                String name1="";
                if(paper==null) name1=complainScholar.getPaperId()+"(paper似乎不存在)";
                else name1=paper.getString("title");
                content+=ft.format(new Date()) +":\n\t您提交对"+name1+"论文"+name+"学者的举报已受理，我们会尽快核实处理，请您耐心等待，谢谢。";
            }else {
                QueryWrapper<User> wrapper2 = new QueryWrapper<>();
                wrapper2.eq("id", complainScholar.getScholarId());
                User user2=userMapper.selectOne(wrapper2);
                String name="";
                if(user2==null) name=complainedid+"(用户似乎不存在)";
                else name=user2.getName();
                content+=ft.format(new Date()) +":\n\t您提交的对"+name+"的用户的举报已受理，我们会尽快核实处理，请您耐心等待，谢谢。";
            }

            complainScholarMapper.updateById(complainScholar);

            notice.setContent(content);
            noticeMapper.insert(notice);//
            return R.success(complainScholar);
        }catch (Exception e) {
            e.printStackTrace();
            return R.fail("未知错误(请检查您的type类型是否正确)");
        }
    }

    /*已测试*/
    @PostMapping("/do-complainscholar")
    @ApiOperation(notes = "需要参数：complainScholar(举报数据id、处理结果类型(0：未处理，1：未通过，2：已通过)、回复)", value = "处理举报用户")
    public R doComplainScholar(@RequestBody JSONObject json,HttpSession session) {
        try{
            if (session.getAttribute("loginUser") == null) {
                return R.fail("此操作需要先登录");
            }
            User user = (User) session.getAttribute("loginUser");
            if (!user.getIsAdmin()) {
                return R.fail("没有管理员权限");
            }

            ComplainScholar complainScholar= new ComplainScholar();
            complainScholar.setId((Integer) json.getInteger("id"));
            complainScholar.setStatus((Integer) json.getInteger("status"));
            complainScholar.setReply((String) json.getString("reply"));
            if(complainScholar==null) return R.fail("没有参数complainScholar");

            QueryWrapper<ComplainScholar> wrapper = new QueryWrapper<>();
            wrapper.eq("id", complainScholar.getId());  //根据id获取举报数据
            ComplainScholar sql = complainScholarMapper.selectOne(wrapper);
            if (null == sql)
                return R.fail("举报数据id不存在");
            sql.setStatus(complainScholar.getStatus());  //把处理结果和回复写上
            sql.setReply(complainScholar.getReply());
            sql.setAuditTime(new Date());  //处理时间

            Notice notice=new Notice();
            notice.setUserId(sql.getUserId());
            notice.setIsRead(false);
            notice.setCreateTime(new Date());
            notice.setContent(sql.getReply());


            if(sql.getStatus()==2){  //通过举报
                if(sql.getType()/10==0){ //用户
//                    Integer state=(Integer) json.getInteger("state");
//                    if(state==null) return R.fail("没有参数state(用户处理类型：1（禁言），2（封号）)");
//
//                    Integer days=(Integer) json.getInteger("days");
//                    if(days==null) return R.fail("没有参数days(用户封禁天数)");
                    Integer state=2;
                    Integer days=3;

                    Integer userid=sql.getScholarId();
                    Calendar c = new GregorianCalendar();
                    Date date = new Date();
                    c.setTime(date);
                    c.add(Calendar.SECOND, days*24*3600);
                    date = c.getTime();

                    scholarService.banuser(date,state,userid);
                }else if(sql.getType()/10==2){  //作者
                    paperService.deleteauthorid(sql.getScholarId(),sql.getAuthorId());
                }else{  //走不到

                }
            }
            complainScholarMapper.update(sql, wrapper);  //更新
            noticeMapper.insert(notice);//
            return R.success(sql);
        }catch (Exception e) {
            e.printStackTrace();
            return R.fail("未知错误");
        }
    }

    /*已测试*/
    @PostMapping("/become-scholar")
    @ApiOperation(notes = "需登录，参数为(作者id)authorId和(邮箱)mail，成功则跳转到/scholar/vercode进行验证码确认", value = "认证学者")
    public R beScholar(@RequestBody JSONObject json, HttpSession session) {
        try {
            if (session.getAttribute("loginUser") == null) {
                return R.fail("此操作需要先登录");
            }
            User user = (User) session.getAttribute("loginUser");
            if (user.getIdentity() == 2) {
                return R.fail("你已经是学者了");
            }
            if (json.get("authorId") == null) {
                return R.fail("作者id不能为空");
            }
            if (json.get("mail")==null){
                return R.fail("邮箱不能为空");
            }

            String mail = (String) json.get("mail");
            QueryWrapper<User> wrapper0 = new QueryWrapper<>();
            wrapper0.eq("mail", mail);
            User user1 = userMapper.selectOne(wrapper0);
            if (user1 != null) {
                return R.fail("邮箱已被使用！如有必要请进行申诉");
            }


            JSONArray papers = paperService.getPaperByAuthorId((String) json.get("authorId"));
            if(papers==null){
                return R.fail("错误的authorId？(根据authorId未找到任何paper)");
            }  //

            if(scholarMapper.selectByAuthorId((String) json.get("authorId"))!=null){
                return R.fail("该authorId已被认领，如果是本人认领请勿再进行认领操作，如果是冒领请进行申诉");
            }
            if(user.getCodeDate()!=null){
                long seconds = ChronoUnit.SECONDS.between(Instant.ofEpochMilli(user.getCodeDate().getTime()),
                        Instant.ofEpochMilli(new Date().getTime()));  //计算时间
                System.out.println(seconds);
                if (seconds < 60) {
                    return R.fail("一分钟内只能获取一次验证码");
                }
            }

            String verCode = mailService.sendEmailVerCode(mail, generateVerCode());

            QueryWrapper<User> wrapper = new QueryWrapper<>();
            wrapper.eq("name", user.getName());
            user.setCodeDate(new Date());  //设置验证码获得时间
            user.setVercode(verCode);
            userMapper.update(user, wrapper);
            session.setAttribute("loginUser", user); //更新session
            session.setAttribute("authorId", json.get("authorId"));
            session.setAttribute("mail", mail);
            System.out.println("用户" + user.getId() + "的验证码为" + user.getVercode());
            return R.success("邮件发送成功");
        } catch (Exception e) {
            e.printStackTrace();
            return R.fail("邮件发送失败");
        }
    }

    /*已测试*/
    @PostMapping("/vercode")
    @ApiOperation(notes = "json传参,传个String类型的vercode代表验证码", value = "认证学者的验证码界面")
    public R vercode(@RequestBody JSONObject json, HttpSession session) {
        try {
            if (session.getAttribute("loginUser") == null) {
                return R.fail("未登录");
            }
            User user = (User) session.getAttribute("loginUser");
            if (session.getAttribute("authorId") == null) {
                return R.fail("作者id已丢失");
            }
            if (session.getAttribute("mail") == null) {
                return R.fail("邮箱已丢失");
            }
            if(user.getIdentity()==2){
                return R.fail("你已经是学者了");
            }
            if (user.getCodeDate() == null) {
                return R.fail("请先获取验证码");
            }
//            if(new Date())
            long seconds = ChronoUnit.SECONDS.between(Instant.ofEpochMilli(user.getCodeDate().getTime()),
                    Instant.ofEpochMilli(new Date().getTime()));  //计算时间
            System.out.println(seconds);
            if (seconds > 600) {
                return R.fail("验证码已过期,请重新获取验证码");
            }
            if (json.get("vercode") == null) {
                return R.fail("验证码不能为空");  //请输入验证码
            }
            String vercode = (String) json.get("vercode");
            if (vercode.equals(user.getVercode())) {
                user.setIdentity(2);  //设置学者身份
                user.setMail((String) session.getAttribute("mail"));

                QueryWrapper<User> wrapper = new QueryWrapper<>();
                wrapper.eq("name", user.getName());
                userMapper.update(user, wrapper);    //更新数据
                session.setAttribute("loginUser", user);    //更新session中数据，应该有必要吧？



                Scholar scholar = new Scholar();  //新建学者对象
                scholar.setId(10);
                scholar.setUserId(user.getId());
                scholar.setClaimTime(new Date());
                scholar.setHotIndex(0);
                scholar.setPaperlist("");
                String authorId = (String) session.getAttribute("authorId");
                if (authorId == null) return R.fail("authorId error");
                JSONObject author= authorService.getAuthorObject(authorId);
                System.out.println(author);



                JSONArray papers = paperService.getPaperByAuthorId(authorId);
                for (Object paper : papers) {
                    System.out.println(((JSONObject) paper).getString("id"));

                    scholar.addpaper(((JSONObject) paper).getString("id"));
                }

                scholar.setAuthorId("");
                scholar.addauthor(authorId);
                session.removeAttribute("authorId");  //用完即扔，防止多次使用
                scholarMapper.insert(scholar);  //应该返回主键

                System.out.println(author.getInteger("h_index")+"\n"+author.getInteger("n_citation")+"\n"+author.getString("name"));
                scholar.setHindex(0);
                scholar.setNcitation(0);
                if(author.getInteger("h_index")!=null) scholar.setHindex(author.getInteger("h_index"));
                if(author.getInteger("n_citation")!=null) scholar.setNcitation(author.getInteger("n_citation"));
                if(author.getString("name")!=null) scholar.setName(author.getString("name"));
                System.out.println(scholar);
                scholarMapper.updateById(scholar);


                QueryWrapper<Scholar> wrapper1 = new QueryWrapper<>();
                wrapper1.eq("user_id", scholar.getUserId());
                Integer result = scholarMapper.selectOne(wrapper1).getId();
                //System.out.println(result);


                ScholarPortal scholarPortal = new ScholarPortal();  //新建学者门户对象
                scholarPortal.setId(1);
                scholarPortal.setScholarId(result);
                scholarPortal.setCount(0);
                scholarPortalMapper.insert(scholarPortal);

                user.setVercode(null);
                user.setCodeDate(null);
                user.setScholarid(result);
                userMapper.update(user,wrapper);  //清除验证码防止重复验证
                session.setAttribute("loginUser",user);


                return R.success(scholar);
            } else {
                return R.fail("验证码错误");
            }


        } catch (Exception e) {
            e.printStackTrace();
            return R.fail("未知错误");
        }
    }

    /*已测试*/
    @PostMapping("/get-paperlist")
    @ApiOperation(notes = "无参数时为获取自己paperlist，需登录，需为学者；有参数scholarId时为获取他人paperlist，无需登录", value = "获取论文列表")
    public R getPaperlist(@RequestBody JSONObject json, HttpSession session) {
        try {
            Scholar scholar;
            if (json.get("scholarId") != null) {   //如果给了scholarId则为获取他人paperlist，无需登录
                //scholarId=(Integer) json.get("scholarId");
                QueryWrapper<Scholar> wrapper = new QueryWrapper<>();
                wrapper.eq("id",json.get("scholarId"));
                scholar = scholarMapper.selectOne(wrapper);

            } else {  //否则为获取自己paperlist，则需已登录且为学者
                if (session.getAttribute("loginUser") == null) {
                    return R.fail("此操作需要先登录");
                }
                User user = (User) session.getAttribute("loginUser");
                if (user.getIdentity() != 2) {
                    return R.fail("非学者");
                }
                QueryWrapper<Scholar> wrapper = new QueryWrapper<>();
                wrapper.eq("user_id", user.getId());
                scholar = scholarMapper.selectOne(wrapper);
            }
            if (scholar == null) {
                return R.fail("这个学者id不存在");
            }
            if (scholar.getPaperlist() == null) return R.fail("paperlist不存在");
            List<String> paperlist = Arrays.asList(scholar.getPaperlist().split(","));
            //return R.success(paperlist);
            return R.success(paperService.getPaperByIDs(new ArrayList<>(paperlist)));

        } catch (Exception e) {
            e.printStackTrace();
            return R.fail("未知错误");
        }
    }

    /*未测试*/
    @PostMapping("/getscholarbyauthorid")
    @ApiOperation(notes = "无需登录，参数为作者id列表：ids", value = "根据作者id获取学者")
    public R getScholarByAuthorId(@RequestBody JSONObject json, HttpSession session) {
        try {
            if (json.get("ids") == null) {
                return R.fail("没有参数ids？");
                //scholarid=(Integer) json.get("scholarid");
            }
            List<String> ids = new ArrayList<>((ArrayList<String>) json.get("ids"));
            List<Scholar> scholars = new ArrayList<>();
            for (String id : ids) {
                Scholar scholar = scholarMapper.selectByAuthorId(id);
                scholars.add(scholar);
            }

            return R.success(scholars);

        } catch (Exception e) {
            e.printStackTrace();
            return R.fail("未知错误");
        }
    }

//    @PostMapping("/getavatarbyauthorid")
//    @ApiOperation(notes = "无需登录，参数为作者id列表：ids", value = "根据作者id获取学者和头像")
//    public R getavatarByAuthorId(@RequestBody JSONObject json, HttpSession session) {
//        try {
//            if (json.get("ids") == null) {
//                return R.fail("没有参数ids？");
//                //scholarid=(Integer) json.get("scholarid");
//            }
//            List<String> ids = new ArrayList<>((ArrayList<String>) json.get("ids"));
////            JSONArray rets=new JSONArray();
//            List<Map<String,Object>> maps=new ArrayList<>();
//            List<Scholar> scholars = new ArrayList<>();
//            for (String id : ids) {
//                Map<String,Object> map = new HashMap<String,Object>();
//                Scholar scholar = scholarMapper.selectByAuthorId(id);
//                if(scholar==null){
//                    map.put("scholar",null);
//                    map.put("avatar", null);
//                    maps.add(map);
//                    continue;
//                }
//                map.put("scholar",scholar);
//                User user=userMapper.selectById(scholar.getUserId());
//                map.put("avatar",user.getAvatar());
//                maps.add(map);
//            }
//
//            return R.success(maps);
//
//        } catch (Exception e) {
//            e.printStackTrace();
//            return R.fail("未知错误");
//        }
//    }

    @PostMapping("/getuserbyauthorid")
    @ApiOperation(notes = "无需登录，参数为作者id列表：ids", value = "根据作者id获取用户id和身份identity")
    public R getUserByAuthorId(@RequestBody JSONObject json, HttpSession session) {
        try {
            if (json.get("ids") == null) {
                return R.fail("没有参数ids？");
                //scholarid=(Integer) json.get("scholarid");
            }
            List<String> ids = new ArrayList<>((ArrayList<String>) json.get("ids"));
            //List<Scholar> scholars=new ArrayList<>();    //
            List<User> users = new ArrayList<>();
            for (String id : ids) {
                Scholar scholar = scholarMapper.selectByAuthorId(id);
                if (scholar == null) {
                    users.add(null);
                    continue;
                }
                //scholars.add(scholar);
                QueryWrapper<User> wrapper = new QueryWrapper<>();
                wrapper.eq("id", scholar.getUserId());
                User usertemp = userMapper.selectOne(wrapper);
                User user = new User();
                user.setId(usertemp.getId());
                user.setIdentity(usertemp.getIdentity());

                user.setLoginDate(null);
                users.add(user);


            }

            return R.success(users);

        } catch (Exception e) {
            e.printStackTrace();
            return R.fail("未知错误");
        }
    }

    @GetMapping("/getallcomplain")
    @ApiOperation(notes = "需登录且为管理员", value = "获取所有举报数据")
    public R getallcomplain(HttpSession session) {
        if (session.getAttribute("loginUser") == null) {
            return R.fail("需要登录且为管理员");
        }
        User user = (User) session.getAttribute("loginUser");
        if (!user.getIsAdmin()) {
            return R.fail("没有管理员权限");
        }

        List<ComplainScholar> all=complainScholarMapper.getall();
        return R.success(all);
    }

    @GetMapping("/getallusercomplain")
    @ApiOperation(notes = "需登录且为管理员", value = "获取所有举报数据")
    public R getallusercomplain(HttpSession session) {
        if (session.getAttribute("loginUser") == null) {
            return R.fail("需要登录且为管理员");
        }
        User user = (User) session.getAttribute("loginUser");
        if (!user.getIsAdmin()) {
            return R.fail("没有管理员权限");
        }

        List<ComplainScholar> all=complainScholarMapper.getalluser();
        return R.success(all);
    }

    @GetMapping("/getallauthorcomplain")
    @ApiOperation(notes = "需登录且为管理员", value = "获取所有举报数据")
    public R getallauthorcomplain(HttpSession session) {
        if (session.getAttribute("loginUser") == null) {
            return R.fail("需要登录且为管理员");
        }
        User user = (User) session.getAttribute("loginUser");
        if (!user.getIsAdmin()) {
            return R.fail("没有管理员权限");
        }

        List<ComplainScholar> all=complainScholarMapper.getallauthor();
        return R.success(all);
    }

}


