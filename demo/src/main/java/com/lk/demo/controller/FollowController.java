package com.lk.demo.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.lk.demo.common.R;
import com.lk.demo.dto.FollowQuery;
import com.lk.demo.dto.FollowResult;
import com.lk.demo.entity.Follow;
import com.lk.demo.entity.Notice;
import com.lk.demo.entity.User;
import com.lk.demo.mapper.FollowMapper;
import com.lk.demo.mapper.NoticeMapper;
import com.lk.demo.mapper.UserMapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.models.auth.In;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/follow")
@Api(description = "关注相关接口")
public class FollowController {
    @Resource
    private FollowMapper followMapper;
    @Resource
    private UserMapper userMapper;
    @Resource
    private NoticeMapper noticeMapper;

    @GetMapping("/fans")
    @ApiOperation(notes = "无说明", value = "获取粉丝列表")
    public R getFans(@RequestParam("userId") Integer userId) {
        QueryWrapper<Follow> wrapper = new QueryWrapper<>();
        wrapper.eq("following_id", userId);
        List<Follow> follows = followMapper.selectList(wrapper);
        List<User> users = new ArrayList<>();
        for (Follow follow : follows)
            users.add(userMapper.selectById(follow.getFollowerId()));
        return R.success(users);
    }

    @GetMapping("")
    @ApiOperation(notes = "无说明", value = "获取关注列表")
    public R get(@RequestParam("userId") Integer userId) {
        QueryWrapper<Follow> wrapper = new QueryWrapper<>();
        wrapper.eq("follower_id", userId);
        List<Follow> follows = followMapper.selectList(wrapper);
        List<User> users = new ArrayList<>();
        for (Follow follow : follows)
            users.add(userMapper.selectById(follow.getFollowingId()));
        return R.success(users);
    }

    @PostMapping("")
    @ApiOperation(notes = "无说明", value = "关注")
    public R post(@RequestBody Follow follow) {
        if (follow.getFollowingId() == null)
            return R.fail("缺少被关注者参数");
        if (follow.getFollowerId() == null)
            return R.fail("缺少关注者参数");
        QueryWrapper<Follow> wrapper = new QueryWrapper<>();
        wrapper.eq("following_id", follow.getFollowingId());
        wrapper.eq("follower_id", follow.getFollowerId());
        if (followMapper.selectOne(wrapper) != null)
            return R.fail("已关注");
        followMapper.insert(follow);
        Notice notice = new Notice();
        notice.setUserId(follow.getFollowingId());
        User user = userMapper.selectById(follow.getFollowerId());
        if (user == null)
            return R.fail("您的id不存在");
        notice.setContent(user.getName() + "关注了您");
        noticeMapper.insert(notice);
        return R.success();
    }

    @DeleteMapping("")
    @ApiOperation(notes = "无说明", value = "取关")
    public R delete(@RequestBody Follow follow) {
        if (follow.getFollowingId() == null)
            return R.fail("缺少被关注者参数");
        if (follow.getFollowerId() == null)
            return R.fail("缺少关注者参数");
        QueryWrapper<Follow> wrapper = new QueryWrapper<>();
        wrapper.eq("following_id", follow.getFollowingId());
        wrapper.eq("follower_id", follow.getFollowerId());
        if (followMapper.delete(wrapper) == 0)
            return R.fail("未关注");
        return R.success();
    }

    @PostMapping("/ask")
    @ApiOperation(notes = "无说明", value = "查询是否关注")
    public R post(@RequestBody FollowQuery followQuery) {
        if (followQuery.getFollowingIds() == null)
            return R.fail("无关注列表参数");
        if (followQuery.getFollowerId() == null)
            return R.fail("无关注者参数");
        Integer followerId = followQuery.getFollowerId();
        ArrayList<FollowResult> followResults = new ArrayList<>();
        for (Integer followingId : followQuery.getFollowingIds()) {
            QueryWrapper<Follow> wrapper = new QueryWrapper<>();
            wrapper.eq("following_id", followingId);
            wrapper.eq("follower_id", followerId);
            if (followMapper.selectOne(wrapper) == null)
                followResults.add(new FollowResult(followingId, false));
            else
                followResults.add(new FollowResult(followingId, true));
        }
        return R.success(followResults);
    }
}
