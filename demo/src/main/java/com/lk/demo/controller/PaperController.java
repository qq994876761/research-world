package com.lk.demo.controller;


import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.lk.demo.common.R;
import com.lk.demo.entity.*;
import com.lk.demo.entity.wrapper.SuggestCondition;
import com.lk.demo.entity.wrapper.paper.PaperMultiSearch;
import com.lk.demo.entity.wrapper.paper.PaperSingleSearch;
import com.lk.demo.entity.wrapper.paper.PaperSuggest;
import com.lk.demo.entity.wrapper.paper.ReturnPapers;
import com.lk.demo.mapper.ComplainPaperMapper;
import com.lk.demo.mapper.NoticeMapper;
import com.lk.demo.mapper.ScholarMapper;
import com.lk.demo.mapper.ViewHistoryMapper;
import com.lk.demo.service.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.lk.demo.common.VerCodeGenerateUtil.getSimilarityRatio;

@RestController
@RequestMapping("/paper")
@Api(description = "学术论文有关接口")
public class PaperController {
    @Resource
    private PaperService paperService;
    @Resource
    private AuthorService authorService;
    @Resource
    private IHotIndexService hotIndexService;
    @Resource
    private IViewHistoryService viewHistoryService;

    @Resource
    private ScholarMapper scholarMapper;
    @Resource
    private NoticeMapper noticeMapper;

    public PaperController(PaperService paperService, DataService dataService) {
        this.paperService = paperService;
        this.dataService = dataService;
    }

    private final DataService dataService;

    /*已测试*/
    @GetMapping("/get-paper")
    @ApiOperation(notes = "无说明", value = "根据ID查找论文")
    public R getPaperByID(@RequestParam("Id") String Id,HttpSession session){
        JSONObject object = paperService.getPaperByID(Id);
        if(object==null){
            return R.fail("not found");
        }
        User user = (User) session.getAttribute("loginUser");
        System.out.println("user:"+user);
        System.out.println("id:"+session.getAttribute("id"));
        if(user!=null){
            viewHistoryService.viewHistory(user.getId(), Id);
        }
        ArrayList<String> fields = (ArrayList<String>) object.get("field");
        hotIndexService.viewPaper(Id,fields.get(0));
        hotIndexService.viewField(fields.get(0));
//        System.out.println(object);
        return R.success(object);
    }
    /*已测试*/
    @GetMapping("/get-all-paper")
    @ApiOperation(notes = "无说明", value = "获取所有论文")
    public R getAllPaper(@RequestParam("size") Integer size,@RequestParam("page") Integer page){
        ReturnPapers object = paperService.getAllPaper(size,page);
        if(object==null){
            return R.fail("not found");
        }
//        System.out.println(object);
        return R.success(object);
    }
    /*已测试*/
    @PostMapping("/simple-search")
    @ApiOperation(notes = "可检索字段需是数据库中已有字段", value = "单条件检索")
    public R searchPaperSimple(@RequestBody PaperSingleSearch singleSearch){
        ReturnPapers object = paperService.searchSimple(singleSearch.getCondition(),singleSearch.getYear(),singleSearch.getSort(), singleSearch.getPaperFilter());
        if(object==null){
            return R.fail("not found");
        }
//        System.out.println(object);
        return R.success(object);
    }
    /*已测试*/
    @PostMapping("/search-theme")
    @ApiOperation(notes = "默认检索方式，通过多字段共同匹配检索\n此处只要给出value。key的值为空即可", value = "主题检索论文")
    public R searchPaperByTheme(@RequestBody PaperSingleSearch singleSearch){
        ReturnPapers objects = paperService.searchByTheme(singleSearch.getCondition(), singleSearch.getYear(), singleSearch.getSort(), singleSearch.getPaperFilter());
        if(objects==null){
            return R.fail("not found");
        }

//        System.out.println(object);
        return R.success(objects);
    }
    /*已测试*/
    @PostMapping("/multi-search")
    @ApiOperation(notes = "参数是oneCondition列表\noption:0是must,1是should,2是not", value = "多条件检索论文")
    public R searchPaperMulti(@RequestBody PaperMultiSearch multiSearch){
        ReturnPapers object = paperService.searchMulti(multiSearch.getConditions(), multiSearch.getYear(), multiSearch.getSort(), multiSearch.getPaperFilter());

        if(object==null){
            return R.fail("not found");
        }
//        System.out.println(object);
        return R.success(object);
    }

    /*已测试*/
    @GetMapping("/add-venue")
    @ApiOperation(notes = "无说明", value = "别用")
    public R addVenue(@RequestParam("pass") Integer pass){
        return R.fail("are you kidding?");
    }

    @PostMapping("/add-index")
    @ApiOperation(notes = "", value = "切勿使用！！！会修改数据库")
    public R addIndex(){
        return R.fail("are you kidding?");
//        dataService.addIndexs();
//        return R.success();
    }
    //@RequestMapping(value="/Author/name={name}",method= RequestMethod.GET)
    //public SearchResult getAuthorByName(@PathVariable String name){
    //    SearchHits hits = authorService.getAuthorByName(name);
    //}


    /*未测试*/
    @PostMapping("/own-paper")
    @ApiOperation(notes = "参数为authorId", value = "根据ID认领论文")
    public R ownPaperByID(@RequestBody JSONObject ob, HttpSession session){
        try{
            if (session.getAttribute("loginUser") == null) {
                return R.fail("未登录");
            }
            User user = ((User) session.getAttribute("loginUser"));
            if(user.getIdentity()!=2){
                return R.fail("请先认证学者");
            }
            if(ob.get("authorId")==null){
                return R.fail("无参数");
            }

            JSONArray papers = paperService.getPaperByAuthorId((String) ob.get("authorId"));
            if(papers==null){
                return R.fail("错误的authorId？(根据authorId未找到任何paper)");
            }

            if(scholarMapper.selectByAuthorId((String) ob.get("authorId"))!=null){
                return R.fail("该authorId已被认领，如果是本人认领请勿再进行认领操作，如果是冒领请进行申诉");
            }

            QueryWrapper<Scholar> wrapper = new QueryWrapper<>();
            wrapper.eq("user_id", user.getId());
            Scholar scholar = scholarMapper.selectOne(wrapper);
            //名字对比
            JSONObject author= authorService.getAuthorObject((String) ob.get("authorId"));
            if(author==null) return R.fail("author为null(不应该啊)");
            System.out.println(author);
            if(getSimilarityRatio(author.getString("name".toLowerCase()),scholar.getName().toLowerCase())<0){
                return R.fail("您的学者信息与该作者信息不符(name相似度太低)");
            }

            for(Object paper:papers){
                System.out.println(((JSONObject) paper).getString("id"));
                scholar.addpaper(((JSONObject) paper).getString("id"));
            }
            scholar.addauthor((String) ob.get("authorId"));

            System.out.println(author.getInteger("h_index")+"\n"+author.getInteger("n_citation")+"\n"+author.getString("name"));
            if(author.getInteger("n_citation")!=null) scholar.setNcitation(scholar.getNcitation()+author.getInteger("n_citation"));
            System.out.println(scholar);
            scholarMapper.updateById(scholar);

            return R.success(scholar);
        }catch (Exception e) {
            e.printStackTrace();
            return R.fail("未知错误");
        }



    }

    @Resource
    private ComplainPaperMapper complainPaperMapper;

//    /*已测试*/
//    @PostMapping("/complain-paper")
//    @ApiOperation(notes = "需要被举报者的id(后端不验证id是否存在)、类型、具体理由", value = "举报论文")
//    public R complainPaper(@RequestBody ComplainPaper complainPaper, HttpSession session){
//        if(session.getAttribute("loginUser")==null){
//            return R.fail("此操作需要先登录");
//        }
//        QueryWrapper<ComplainPaper> wrapper = new QueryWrapper<>();
//        complainPaper.setStatus(0);  //未处理
//        complainPaper.setUserId(((User) session.getAttribute("loginUser")).getId());
//        complainPaper.setCreateTime(new Date());
//        complainPaper.setId(11);
//
//        complainPaperMapper.insert(complainPaper);
//        return R.success(complainPaper);
//    }

    /*已测试*/
    @PostMapping("/complain-paper")
    @ApiOperation(notes = "需要被举报者的id(后端不验证id是否存在)、类型、具体理由", value = "举报论文")
    public R complainPaper(@RequestParam(value = "type", required=false) Integer type,
                           @RequestParam(value = "reason", required=false) String reason,
                           @RequestParam(value = "paperId", required=false) String paperId,
                           @RequestParam(value = "file", required=false) MultipartFile file,
                           HttpSession session, HttpServletRequest request){
        try {
            if(session.getAttribute("loginUser")==null){
                return R.fail("此操作需要先登录");
            }
            User user=(User) session.getAttribute("loginUser");
//            if(complainPaper==null){
//                return R.fail("没有举报数据") ;
//            }
//            Integer type=complainPaper.getType();
//            String paperId=complainPaper.getPaperId();
            ComplainPaper complainPaper=new ComplainPaper();

            complainPaper.setType(type);
            complainPaper.setReason(reason);
            complainPaper.setPaperId(paperId);
            if(type==null || paperId==null){
                return R.fail("缺少参数");
            }


            QueryWrapper<ComplainScholar> wrapper = new QueryWrapper<>();
            complainPaper.setStatus(0);  //未处理
            complainPaper.setUserId(user.getId());

            complainPaper.setCreateTime(new Date());
            //complainScholar.setId(11);
            complainPaperMapper.insert(complainPaper);

            if(file!=null){  //如果传了举报截图
                System.out.println("上传截图");
                String dirPath = request.getServletContext().getRealPath("upload");//会在webapp下面创建此文件夹
                System.out.println("dirPath=" + dirPath);

                File dir = new File(dirPath);
                if (!dir.exists()) {
                    dir.mkdirs();
                }
                //2.确定保存的文件名
                String orginalFilename = file.getOriginalFilename();
                int beginIndex = orginalFilename.lastIndexOf(".");
                String suffix = "";
                if (beginIndex != -1) {
                    suffix = orginalFilename.substring(beginIndex);
                }
                String filename = "举报论文截图"+complainPaper.getId().toString() + suffix;
                //创建文件对象，表示要保存的头像文件,第一个参数表示存储的文件夹，第二个参数表示存储的文件
                File dest = new File(dir, filename);
                //执行保存
                file.transferTo(dest);
                String avatar = "/upload/" + filename;
                complainPaper.setImage(avatar);
                complainPaperMapper.updateById(complainPaper);
            }


            Notice notice=new Notice();
            notice.setUserId(user.getId());
            notice.setIsRead(false);
            notice.setCreateTime(new Date());
            //notice.setId(1);
            String content="";
            SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

            JSONObject paper= paperService.getPaperByID(complainPaper.getPaperId());
            String name1="";
            if(paper==null) name1=complainPaper.getPaperId()+"(paper似乎不存在)";
            else name1=paper.getString("title");
            content+=ft.format(new Date()) +":\n\t您提交对"+name1+"论文的举报已受理，我们会尽快核实处理，请您耐心等待，谢谢。";


            notice.setContent(content);
            noticeMapper.insert(notice);//
            return R.success(complainPaper);
        }catch (Exception e) {
            e.printStackTrace();
            return R.fail("未知错误");
        }
    }

    /*已测试*/
    @PostMapping("/do-complainpaper")
    @ApiOperation(notes = "需要举报数据id、处理结果类型、回复", value = "处理举报论文")
    public R doComplainPaper(@RequestBody ComplainPaper complainPaper,HttpSession session){
        if(session.getAttribute("loginUser")==null){
            return R.fail("此操作需要先登录");
        }
        User user = (User) session.getAttribute("loginUser");
        if(!user.getIsAdmin()){
            return R.fail("没有管理员权限");
        }
        QueryWrapper<ComplainPaper> wrapper = new QueryWrapper<>();
        wrapper.eq("id", complainPaper.getId());  //根据id获取举报数据
        ComplainPaper sql = complainPaperMapper.selectOne(wrapper);
        if(null == sql)
            return R.fail("举报数据id不存在");
        sql.setStatus(complainPaper.getStatus());  //把处理结果和回复写上
        sql.setReply(complainPaper.getReply());
        sql.setAuditTime(new Date());  //处理时间
        complainPaperMapper.update(sql,wrapper);  //更新

        Notice notice=new Notice();
        notice.setUserId(sql.getUserId());
        notice.setIsRead(false);
        notice.setCreateTime(new Date());
        notice.setContent(sql.getReply());
        noticeMapper.insert(notice);//

        return R.success(complainPaper);
    }

    @ApiOperation(value = "自动补全", notes = "用于在用户输入时给出自动补全的建议，" +
            "必须包含prefix字段，表示用户已经输入的内容。" +
            "可选字段包括size字段，表示每一个类别中自动补全的建议数，默认为5，最大为10。" +
            "返回值data为一个json对象，其中包含1个字段，为title，是论文标题的补全建议")
    @PostMapping(value="/suggest")
    public R getPaperSuggest(@RequestBody PaperSuggest cond){
        if(cond.prefix.equals(""))
            return R.fail("need prefix");
        JSONArray title = paperService.suggest(cond.prefix,"title",cond.size);
//        JSONArray keywords = paperService.suggest(cond.prefix,"keywords",cond.size);
        if(title==null)
            return R.fail("no suggestion");
        JSONObject ans = new JSONObject();
        ans.put("title",title);
        return R.success(ans);
    }

    @GetMapping("/getallcomplain")
    @ApiOperation(notes = "需登录且为管理员", value = "获取所有举报数据")
    public R getallcomplain(HttpSession session) {
        if (session.getAttribute("loginUser") == null) {
            return R.fail("需要登录且为管理员");
        }
        User user = (User) session.getAttribute("loginUser");
        if (!user.getIsAdmin()) {
            return R.fail("没有管理员权限");
        }

        List<ComplainPaper> all=complainPaperMapper.getall();
        return R.success(all);
    }




    class SearchResult{
        @JsonInclude
        ArrayList<String> result;
        @JsonInclude
        String info;

        public SearchResult(ArrayList<String> result,String info){
            this.result = result;
            this.info = info;
        }

        public String getResult(){
            StringBuilder s= new StringBuilder();
            s.append("[");
            for(int i=0;i<result.size();++i){
                if(i != 0)
                    s.append(",");
                s.append(result.get(i));
            }
            s.append("]");
            return s.toString();
        }

        public String getInfo(){
            return info;
        }
    }
}
