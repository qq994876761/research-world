package com.lk.demo.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.lk.demo.common.R;
import com.lk.demo.entity.Collection;
import com.lk.demo.entity.Comment;
import com.lk.demo.entity.Like;
import com.lk.demo.mapper.CommentMapper;
import com.lk.demo.mapper.LikeMapper;
import com.lk.demo.service.PaperService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@RestController
@RequestMapping("/like")
@Api(description = "点赞相关接口")
public class LikeController {
    @Resource
    private LikeMapper likeMapper;
    @Resource
    private CommentMapper commentMapper;

    @PostMapping("like")
    @ApiOperation(notes = "无说明", value = "点赞评论")
    public R like(@RequestBody Like like) {
        QueryWrapper<Like> wrapper = new QueryWrapper<>();
        wrapper.eq("user_id", like.getUserId());
        wrapper.eq("comment_id", like.getCommentId());
        if (likeMapper.selectOne(wrapper) != null)
            return R.fail("已点赞");
        likeMapper.insert(like);
        Comment comment = commentMapper.selectById(like.getCommentId());
        comment.setLikes(comment.getLikes() + 1);
        commentMapper.updateById(comment);
        return R.success();
    }

    @PostMapping("dislike")
    @ApiOperation(notes = "无说明", value = "取消点赞")
    public R dislike(@RequestBody Like like) {
        QueryWrapper<Like> wrapper = new QueryWrapper<>();
        wrapper.eq("user_id", like.getUserId());
        wrapper.eq("comment_id", like.getCommentId());
        if (likeMapper.delete(wrapper) == 0)
            return R.fail("未点赞");
        Comment comment = commentMapper.selectById(like.getCommentId());
        comment.setLikes(comment.getLikes() - 1);
        commentMapper.updateById(comment);
        return R.success();
    }
}
