package com.lk.demo.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.lk.demo.common.R;
import com.lk.demo.dto.ViewHistoryDTO;
import com.lk.demo.entity.ViewHistory;
import com.lk.demo.mapper.ViewHistoryMapper;
import com.lk.demo.service.PaperService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@RestController
@RequestMapping("/viewHistory")
@Api(description = "浏览历史相关接口")
public class ViewHistoryController {
    @Resource
    private ViewHistoryMapper viewHistoryMapper;
    @Resource
    private PaperService paperService;

    @GetMapping("")
    @ApiOperation(notes = "无说明", value = "获取浏览历史列表")
    public R get(@RequestParam("userId") Integer userId) {
        if (userId == null)
            return R.fail("缺少用户id参数");
        QueryWrapper<ViewHistory> wrapper = new QueryWrapper<>();
        wrapper.eq("user_id", userId);
        List<ViewHistory> viewHistories = viewHistoryMapper.selectList(wrapper);
        Collections.sort(viewHistories);
        ArrayList<ViewHistoryDTO> viewHistoryDTOS = new ArrayList<>();
        for (ViewHistory viewHistory : viewHistories)
            viewHistoryDTOS.add(new ViewHistoryDTO(viewHistory, paperService.getPaperByID(viewHistory.getPaperId())));
        return R.success(viewHistoryDTOS);
    }

//    @PostMapping("")
//    @ApiOperation(notes = "无说明", value = "添加浏览历史记录")
//    public R post(@RequestBody ViewHistory viewHistory) {
//        if (viewHistory.getUserId() == null)
//            return R.fail("缺少用户id参数");
//        if (viewHistory.getPaperId() == null)
//            return R.fail("缺少论文id参数");
//        QueryWrapper<ViewHistory> wrapper = new QueryWrapper<>();
//        wrapper.eq("user_id", viewHistory.getUserId());
//        wrapper.eq("paper_id", viewHistory.getPaperId());
//        viewHistoryMapper.insert(viewHistory);
//        return R.success();
//    }

    @DeleteMapping("")
    @ApiOperation(notes = "无说明", value = "删除浏览历史记录")
    public R delete(@RequestBody ViewHistory viewHistory) {
        if (viewHistory.getUserId() == null)
            return R.fail("缺少用户id参数");
        if (viewHistory.getPaperId() == null)
            return R.fail("缺少论文id参数");
        QueryWrapper<ViewHistory> wrapper = new QueryWrapper<>();
        wrapper.eq("user_id", viewHistory.getUserId());
        wrapper.eq("paper_id", viewHistory.getPaperId());
        if (viewHistoryMapper.delete(wrapper) == 0)
            return R.fail("无对应浏览历史记录");
        return R.success();
    }

    @DeleteMapping("/id")
    @ApiOperation(notes = "无说明", value = "通过id删除浏览历史记录")
    public R deleteById(@RequestParam("viewHistoryId") Integer viewHistoryId) {
        if (viewHistoryId == null)
            return R.fail("浏览历史id为空");
        if (viewHistoryMapper.deleteById(viewHistoryId) == 0)
            return R.fail("无对应浏览历史记录");
        return R.success();
    }
}
