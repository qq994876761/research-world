package com.lk.demo.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.lk.demo.common.R;
import com.lk.demo.entity.Collection;
import com.lk.demo.mapper.CollectionMapper;
import com.lk.demo.service.PaperService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/collection")
@Api(description = "收藏相关接口")
public class CollectionController {
    @Resource
    private CollectionMapper collectionMapper;
    @Resource
    private PaperService paperService;

    @GetMapping("")
    @ApiOperation(notes = "无说明", value = "获取用户收藏列表")
    public R get(@RequestParam("userId") Integer userId) {
        QueryWrapper<Collection> wrapper = new QueryWrapper<>();
        wrapper.eq("user_id", userId);
        List<Collection> collections = collectionMapper.selectList(wrapper);
        ArrayList<String> paperIds = new ArrayList<>();
        for (Collection collection : collections)
            paperIds.add(collection.getPaperId());
        return R.success(paperService.getPaperByIDs(paperIds));
    }

    @PostMapping("")
    @ApiOperation(notes = "无说明", value = "收藏学术成果")
    public R post(@RequestBody Collection collection) {
        QueryWrapper<Collection> wrapper = new QueryWrapper<>();
        wrapper.eq("user_id", collection.getUserId());
        wrapper.eq("paper_id", collection.getPaperId());
        if (collectionMapper.selectOne(wrapper) != null)
            return R.fail("已收藏");
        collectionMapper.insert(collection);
        return R.success();
    }

    @DeleteMapping("")
    @ApiOperation(notes = "无说明", value = "取消收藏学术成果")
    public R delete(@RequestBody Collection collection) {
        QueryWrapper<Collection> wrapper = new QueryWrapper<>();
        wrapper.eq("user_id", collection.getUserId());
        wrapper.eq("paper_id", collection.getPaperId());
        if (collectionMapper.delete(wrapper) == 0)
            return R.fail("未收藏");
        return R.success();
    }
}
