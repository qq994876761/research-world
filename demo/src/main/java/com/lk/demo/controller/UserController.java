package com.lk.demo.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.lk.demo.common.R;
import com.lk.demo.entity.User;
import com.lk.demo.mapper.UserMapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.boot.system.ApplicationHome;
import org.springframework.util.DigestUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/user")
@Api(description = "普通用户相关接口")
public class UserController {
    @Resource
    private UserMapper userMapper;

    @PostMapping("/register")
    @ApiOperation(notes = "(不需要邮箱了！！！！)需要用户名、密码，用户名不能重复", value = "注册")
    public R register(@RequestBody User dto) {
        String username = dto.getName();
        if(username == null)
            return R.fail("缺少用户名参数");
        if(dto.getPwd()==null)
            return R.fail("缺少密码参数");
        dto.setPwd(DigestUtils.md5DigestAsHex(dto.getPwd().getBytes()));
        //String password= dto.getPwd();
//        String mail = dto.getMail();
//        QueryWrapper<User> wrapper = new QueryWrapper<>();
//        wrapper.eq("mail", mail);
//        User user = userMapper.selectOne(wrapper);
        QueryWrapper<User> wrapper1 = new QueryWrapper<>();
        wrapper1.eq("name", username);
        User user1 = userMapper.selectOne(wrapper1);
//        String formatOfEmail = "([0-9]|[a-z]|[A-Z]|@)+.(com|cn)";

//        String formatOfEmail = "[a-zA-Z0-9_-]+(.[a-zA-Z0-9_-]+)+";

//        if (!Pattern.matches(formatOfEmail, mail)) {
//            return R.fail("邮箱格式错误！");
//        } else if (user != null) {
//            System.out.println("failmail\n" + user.toString());
//            return R.fail("邮箱已注册！");
//        } else
        if (user1 != null) {
            System.out.println("failname\n" + user1.toString());
            return R.fail("用户名已注册！");
        } else {
//            QueryWrapper<User> wrapper2 = new QueryWrapper<>();
//            User user3 = userMapper.selectOne(wrapper2.orderByDesc("id").last("limit 1"));
//            dto.setId(user3.getId()+1);    //没id会报错，试过像小学期那样改成自动增长，没成功。。。影响性能，先用着，回头再改
//            dto.setId(null);
            dto.setIdentity(1);//普通用户
            //dto.setId(1);//没用，但是要有
            dto.setState(0); //未封禁
            dto.setIsAdmin(false);

            userMapper.insert(dto);

            return R.success(dto);
        }
    }

    @PostMapping("/login")
    @ApiOperation(notes = "使用用户密码", value = "登录")
    public R login(@RequestBody User user, HttpSession session) {

        String name = user.getName();
        String password = user.getPwd();
        Map<String, Object> map = new HashMap<>();
        try {
            QueryWrapper<User> wrapper = new QueryWrapper<>();
            wrapper.eq("name", name);
            User user1 = userMapper.selectOne(wrapper);

            if (user1 != null) {
                if (user1.getState() == 2) {
                    long seconds = ChronoUnit.SECONDS.between(Instant.ofEpochMilli(user1.getBanDate().getTime()),
                            Instant.ofEpochMilli(new Date().getTime()));  //计算时间
                    System.out.println(seconds);
                    if (seconds < 0) {
                        return R.fail("用户已被封禁至"+user1.getBanDate());
                    }
                    else {
                        user1.setState(0);
                        user1.setBanDate(null);
                    }
                }

                if (DigestUtils.md5DigestAsHex(user.getPwd().getBytes()).equals(user1.getPwd())) {
                    user1.setLoginDate(new Date());
                    userMapper.update(user1, wrapper);  //更新登录时间
                    session.setAttribute("loginUser", user1);
                    session.setMaxInactiveInterval(60 * 60);  //

                    return R.success(user1);
                } else {
                    return R.fail("密码错误");
                }
            } else {
                return R.fail("不存在该用户名");
            }
        } catch (Exception e) {
            e.printStackTrace();
            return R.fail("未知错误");
        }
    }

    @PostMapping("/logout")
    @ApiOperation(notes = "无说明", value = "登出")
    public R Logout(HttpSession session) {
        Map<String, Object> map = new HashMap<>();
        try {
            if (session.getAttribute("loginUser") == null) {
                return R.fail("未登录");
            }
            session.invalidate();
            return R.success("登出成功");
        } catch (Exception e) {
            e.printStackTrace();
            return R.fail("登出失败");
        }
    }

    @GetMapping("")
    @ApiOperation(notes = "无说明", value = "获取用户信息")
    public R get(@RequestParam("userId") Integer userId) {
        User user = userMapper.selectById(userId);
        if (null == user)
            return R.fail("用户id不存在");
        return R.success(user);
    }

    @PutMapping("/update")
    @ApiOperation(notes = "无说明", value = "编辑用户信息")
    public R update(@RequestBody User user) {
        if(user.getId() == null)
            return R.fail("缺少id参数");
        if(user.getPwd() != null)
            user.setPwd(DigestUtils.md5DigestAsHex(user.getPwd().getBytes()));
        //user.setPwd(null);
        if (userMapper.updateById(user) == 0)
            return R.fail("用户id不存在");
        return R.success();
    }

    @PostMapping("/alterPassword")
    @ApiOperation(notes = "三个param", value = "修改密码")
    public R alterPassword(@RequestParam(value = "userId") Integer userId, @RequestParam(value = "oldPassword") String oldPassword, @RequestParam(value = "newPassword") String newPassword) {
        if (userId == null)
            return R.fail("用户id为空");
        if (oldPassword == null)
            return R.fail("原密码为空");
        if (newPassword == null)
            return R.fail("新密码为空");
        User user = userMapper.selectById(userId);
        if (user == null)
            return R.fail("用户id不存在");
        //System.out.println(user.getPwd()+" "+DigestUtils.md5DigestAsHex(oldPassword.getBytes()));
        if (!user.getPwd().equals(DigestUtils.md5DigestAsHex(oldPassword.getBytes())))
            return R.fail("原密码错误");
        user.setPwd(DigestUtils.md5DigestAsHex(newPassword.getBytes()));
        if (userMapper.updateById(user) == 0)
            return R.fail("修改失败");
        return R.success();
    }

    @PutMapping("/setAvatar")
    @ApiOperation(notes = "无说明", value = "设置头像")
    public R setAvatar(@RequestParam(value = "userId") Integer userId, @RequestParam(value = "file") MultipartFile file, HttpServletRequest request) {
        try {
            String dirPath;
            String os = System.getProperty("os.name");
            if (os.toLowerCase().startsWith("win")) {  //如果是Windows系统
                dirPath = "F:/avatar/";
            } else {//linux和mac系统
                ApplicationHome h = new ApplicationHome(getClass());
                File jarF = h.getSource();
                dirPath = jarF.getParentFile().toString()+"/avatar/";
            }
            //1.确定保存的文件夹
            System.out.println("dirPath=" + dirPath);

            File dir = new File(dirPath);
            if (!dir.exists()) {
                dir.mkdirs();
            }
            //2.确定保存的文件名
            String orginalFilename = file.getOriginalFilename();
            int beginIndex = orginalFilename.lastIndexOf(".");
            String suffix = "";
            if (beginIndex != -1) {
                suffix = orginalFilename.substring(beginIndex);
            }
            String filename = userId.toString() + suffix;
            //创建文件对象，表示要保存的头像文件,第一个参数表示存储的文件夹，第二个参数表示存储的文件
            File dest = new File(dir, filename);
            //执行保存
            file.transferTo(dest);
            //更新数据表
            String avatar = "/avatar/" + filename;
            // 通过uid找到用户
            User user = new User();
            user.setAvatar(avatar);
            user.setId(userId);
            userMapper.updateById(user);
            return R.success();
        } catch (Exception e) {
            e.printStackTrace();
            return R.fail("上传失败");
        }
    }
}