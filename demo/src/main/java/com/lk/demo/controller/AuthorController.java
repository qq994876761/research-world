package com.lk.demo.controller;


import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.lk.demo.common.R;
import com.lk.demo.entity.wrapper.MultiSearchCondition;
import com.lk.demo.entity.wrapper.SimpleSearchCondition;
import com.lk.demo.entity.wrapper.SuggestCondition;
import com.lk.demo.service.AuthorService;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.lk.demo.service.ScholarService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

@Api("作者相关接口")
@RestController
@EnableCaching
@RequestMapping("/author")
public class AuthorController {
    private final AuthorService authorService;
    private final ScholarService scholarService;

    @Autowired
    public AuthorController(AuthorService authorService, ScholarService scholarService){
        this.authorService = authorService;
        this.scholarService = scholarService;
    }

    @ApiOperation(value = "通过ID获取作者", notes = "通过提供ID获取作者信息," +
            "返回结果可能为500 error,500 not found,200 data中包含作者信息")
    @PostMapping(value="/id")
    @Cacheable(value="EsCache",key="'AuthorID'+#ID")
    public R getAuthorByID(@RequestParam("ID") String ID){
        JSONArray ja = authorService.getAuthorByID(ID);
        System.out.println("run");
        if(ja == null){
            return R.fail("error");
        }else{
            if(ja.isEmpty())
                return R.fail("not found");
            return R.success(ja.getJSONObject(0));
        }
    }

    @ApiOperation(value = "通过ID列表获取多个作者", notes = "通过提供ID列表获取多个作者信息," +
            "返回结果可能为500 error,500 not found,200 data中包含作者信息")
    @PostMapping(value="/ids")
    public R getAuthorByIDs(@RequestParam("IDs") ArrayList<String> IDs){
        JSONArray ja = authorService.getAuthorByIDs(IDs);
        if(ja == null){
            return R.fail("error");
        }else{
            return R.success(ja);
        }
    }

    @ApiOperation(value = "简单检索作者", notes = "不指定匹配字段的简单检索,将在标签，姓名，组织中同时进行检索，" +
            "需要提供一个cond的json对象，其中必须包含value字段表示欲检索的信息，" +
            "可以包含size字段表示返回信息中包含的结果数，默认值为20，page字段表示当前为第几页，默认值为0")
    @PostMapping(value="/simpleSearch")
    @Cacheable(value="EsCache",key="'AuthorSimpleSearch'+#cond.value+'size'+#cond.size+'page'+#cond.page")
    public R authorSimpleSearchController(@RequestBody SimpleSearchCondition cond){
        JSONArray ja = authorService.authorSimpleSearchService(cond);
        if(ja == null){
            return R.fail("error");
        }else{
            if(ja.isEmpty())
                return R.fail("not found");
            return R.success(ja);
        }
    }

    @ApiOperation(value = "多条件检索作者", notes = "可以进行与或非逻辑组合的条件检索，需要提供一个condition的json对象，" +
            "其中包含must，should，must_not三个列表，列表中的元素为json对象，" +
            "其中包含key字段，表示检索的字段，可以为orgs,tags或name，" +
            "包含value字段表示检索的信息。三个列表不能全部为空。" +
            "此外condition中还可以包含size字段表示返回信息中包含的结果数，默认值为20，page字段表示当前为第几页，默认值为0")
    @PostMapping(value="/multiSearch")
    public R authorMultiSearchController(@RequestBody MultiSearchCondition cond){
        JSONArray ja = authorService.authorMultiSearchService(cond);
        if(ja == null){
            return R.fail("error");
        }else{
            if(ja.isEmpty())
                return R.fail("not found");
            return R.success(ja);
        }
    }

    @ApiOperation(value = "获取作者关系网络", notes = "在查询作者信息时会获取到relation字段，若其中没有值，" +
            "可能是之前没有计算过，此时应向此接口发出请求，包含一个字段ID为作者ID，查询会返回相关的作者名字，ID以及合作次数")
    @PostMapping(value="/relation")
    @Cacheable(value="EsCache",key="'AuthorRelation'+#ID")
    public R getAuthorRelation(@RequestParam("ID") String ID){
        JSONArray relation = authorService.getAuthorRelationService(ID);
        if(relation == null)
            return R.fail("not found");
        ArrayList<String> ids = new ArrayList<>();
        for(int i=0;i< relation.size();i++){
            ids.add(relation.getJSONObject(i).getString("id"));
        }
        ArrayList<HashMap<String,Object>> scholars = scholarService.getavatarByAuthorId(ids);
        JSONArray returnJson = new JSONArray();
        for(int i=0;i< relation.size();i++){
            if(scholars.get(i).get("scholar")==null){
                returnJson.add(relation.getJSONObject(i));
            }
            else{
                returnJson.add(scholars.get(i));
            }
        }
        System.out.println(returnJson);
        return R.success(returnJson);
    }

    @ApiOperation(value = "自动补全", notes = "注：由于es容量原因，此接口暂时删除\n用于在用户输入时给出自动补全的建议，" +
            "必须包含prefix字段，表示用户已经输入的内容。" +
            "可选字段包括size字段，表示每一个类别中自动补全的建议数，默认为5，最大为10。" +
            "field字段，可以用于指定自动补全的类别，可以为name,orgs,tags,若留空，则将在三个类别中各给出size个补全建议。" +
            "返回值data为一个json对象，其中包含三个字段，为name,orgs,tags，是三个String类型的列表,为三个类别的补全建议")
    @PostMapping(value="/suggest")
    public R getSuggest(@RequestBody SuggestCondition cond){
        if(cond.prefix.equals(""))
            return R.fail("need prefix");
        if(!cond.field.equals("")){
            if(cond.field.equals("tags")){
                cond.field = "tags.t";
            }
            JSONArray ja = authorService.suggest(cond.prefix,cond.field,cond.size);
            if(ja == null)
                return R.fail("no suggestion");
            JSONObject ans = new JSONObject();
            if(cond.field.equals("tags.t")){
                cond.field = "tags";
            }
            ans.put(cond.field,ja);
            return R.success(ans);
        }else{
            JSONArray tag = authorService.suggest(cond.prefix,"tags.t",cond.size);
            JSONArray name = authorService.suggest(cond.prefix,"name",cond.size);
            JSONArray org = authorService.suggest(cond.prefix,"orgs",cond.size);
            if(tag == null&&name==null&&org ==null)
                return R.fail("no suggestion");
            JSONObject ans = new JSONObject();
            ans.put("tags",tag);
            ans.put("name",name);
            ans.put("orgs",org);
            return R.success(ans);
        }
    }

}
