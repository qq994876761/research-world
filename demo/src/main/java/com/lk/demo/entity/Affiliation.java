package com.lk.demo.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
public class Affiliation {
    @TableId
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id=0;
    private String name;
    private String bio;
    private String logo;
    private Integer hotIndex;
    @Column(length=4095)
    private String scholars;
    @Column(length=4095)
    private String numPubsPerYear;
    @Column(length=4095)
    private String fields;
}
