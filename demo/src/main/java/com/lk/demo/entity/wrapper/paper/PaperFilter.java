package com.lk.demo.entity.wrapper.paper;

public class PaperFilter {
    private boolean highSite=false;
    private boolean hot=false;
    private boolean canGet=false;

    public boolean isHighSite() {
        return highSite;
    }

    public void setHighSite(boolean highSite) {
        this.highSite = highSite;
    }

    public boolean isHot() {
        return hot;
    }

    public void setHot(boolean hot) {
        this.hot = hot;
    }

    public boolean isCanGet() {
        return canGet;
    }

    public void setCanGet(boolean canGet) {
        this.canGet = canGet;
    }
}
