package com.lk.demo.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
public class Scholar implements Serializable {
    @Id
    @TableId
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id = 0;
    private Integer userId;
    @Column(length=4095)
    private String authorId;    //格式为 id,id,id,id
    private String name;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date claimTime;
    private Integer hotIndex;
    private String field;   //领域
    private String affi;    //机构
    private Integer ncitation;   //被引用总数
    private Integer hindex;  //h因子
    @Column(length=4095)
    private String paperlist;  //格式为 paperid,paperid,paperid,paperid




    public List<String> getpaper() {
        if (paperlist == null)
            return new ArrayList<>();
        List<String> list = new ArrayList<>(Arrays.asList(paperlist.split(",")));
        return list;
    }

    public void addpaper(String id) {
        if(paperlist==null) paperlist="";
        if (paperlist != null && !paperlist.contains(id)) {
            if (!paperlist.equals(""))
                paperlist += ",";
            paperlist += id;
        }
    }

    public void deletepaper(String id) {
        if(paperlist==null || !paperlist.contains(id)) return;
        //没有验证id正确性，前端应该可以保证吧
        List<String> list = new ArrayList<>();
        if (paperlist != null)
            list = new ArrayList<>(Arrays.asList(paperlist.split(",")));
        list.remove(id);
        StringBuilder str = new StringBuilder();
        for (String temp : list) {
            str.append(temp).append(",");
        }
        if (list.size() >= 1) str = new StringBuilder(str.substring(0, str.length() - 1));
        paperlist = String.valueOf(str);
    }

    public List<String> Getauthor() {
        if (authorId == null)
            return new ArrayList<>();
        List<String> list = new ArrayList<>(Arrays.asList(authorId.split(",")));
        return list;
    }

    public void addauthor(String id) {
        if(authorId==null) authorId="";
        if (authorId != null && !authorId.contains(id)) {
            if (!authorId.equals(""))
                authorId += ",";
            authorId += id;
        }
    }

    public void deleteauthor(String id) {
        if(authorId==null ||  !authorId.contains(id)) return;
        //没有验证id正确性，前端应该可以保证吧
        List<String> list = new ArrayList<>();
        if (authorId != null)
            list = new ArrayList<>(Arrays.asList(authorId.split(",")));
        list.remove(id);
        StringBuilder str = new StringBuilder();
        for (String temp : list) {
            str.append(temp).append(",");
        }
        if (list.size() >= 1) str = new StringBuilder(str.substring(0, str.length() - 1));
        authorId = String.valueOf(str);
    }

}
