package com.lk.demo.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
public class ComplainScholar {
    @Id
    @TableId
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id=0;     //举报数据id

    private String complainedid;   //前端用的id
    private Integer userId;   //举报者id
    private Integer scholarId;   //被举报id，分举报用户和举报学者
    private String authorId;    //举报作者时需要,其他时候为null

    private String paperId;    //举报学者时需要，其他时候为null

    private Integer type;  //1是用户评论违规，2是用户私信违规，11是学者言论违规，12是学者行为违规，21是作者id被冒领。。。
    private String reason;    //具体原因
    private String image;  //举报截图

    private Integer status;
    //0：未处理，1：未通过，2：已通过
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date auditTime;
    private String reply;    //管理员回复
}
