package com.lk.demo.entity.wrapper.paper;

public class Year {
    private Integer l=0;
    private Integer r=9999;

    public Integer getL() {
        return l;
    }

    public void setL(Integer l) {
        this.l = l;
    }

    public Integer getR() {
        return r;
    }

    public void setR(Integer r) {
        this.r = r;
    }
}
