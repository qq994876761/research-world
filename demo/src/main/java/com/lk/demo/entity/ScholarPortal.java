package com.lk.demo.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
public class ScholarPortal {
    @Id
    @TableId
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id = 0;
    private Integer scholarId;
    private String bgp;
    private Integer count;
    @Column(length=4095)
    private String paperShow;


    public void addpaper(String id){
        if(paperShow==null) paperShow="";
        if(paperShow!=null && !paperShow.contains(id)){
            if(!paperShow.equals(""))
                paperShow+=",";
            paperShow+=id;
        }
    }

    public void deletepaper(String id){
        //没有验证id正确性，前端应该可以保证吧
        if(paperShow==null || !paperShow.contains(id)) return;
        List<String> list = new ArrayList<>(Arrays.asList(paperShow.toString().split(",")));
        list.remove(id);
        StringBuilder str = new StringBuilder();
        for (String temp : list) {
            str.append(temp).append(",");
        }
        if (list.size() >= 1) str = new StringBuilder(str.substring(0, str.length() - 1));
        paperShow = String.valueOf(str);

    }

    public List<String> Getpaper(){
        if(paperShow==null || paperShow.equals("")) return new ArrayList<>();
        List<String> list= new ArrayList<>(Arrays.asList(paperShow.toString().split(",")));
        return list;
    }
}
