package com.lk.demo.entity.wrapper;

public class SimpleSearchCondition {
    public Integer size = 20;
    public Integer page = 0;
    public String value;

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

}
