package com.lk.demo.entity.wrapper.paper;

import com.alibaba.fastjson.JSONArray;

public class ReturnPapers {
    private JSONArray jsonArray;
    private JSONArray relaAuthor;
    private Long total=0L;

    public ReturnPapers(JSONArray jsonArray, Long total){
        this.jsonArray=jsonArray;
        this.total=total;
    }
    public ReturnPapers(JSONArray jsonArray, Long total,JSONArray relaAuthor){
        this.jsonArray=jsonArray;
        this.total=total;
        this.relaAuthor = relaAuthor;
    }
    public JSONArray getJsonArray() {
        return jsonArray;
    }

    public void setJsonArray(JSONArray jsonArray) {
        this.jsonArray = jsonArray;
    }

    public Long getTotal() {
        return total;
    }

    public void setTotal(Long total) {
        this.total = total;
    }

    public JSONArray getRelaAuthor() {
        return relaAuthor;
    }

    public void setRelaAuthor(JSONArray relaAuthor) {
        this.relaAuthor = relaAuthor;
    }
}
