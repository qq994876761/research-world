package com.lk.demo.entity.wrapper;

public class SuggestCondition {
    public String prefix = "";
    public int size = 5;
    public String field = "";

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        if(size>10)
            size = 10;
        if(size<0)
            size = 5;
        this.size = size;
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        if(field.equals("tags")||field.equals("name")||field.equals("orgs"))
            this.field = field;
    }
}
