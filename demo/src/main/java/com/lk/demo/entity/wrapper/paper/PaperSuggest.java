package com.lk.demo.entity.wrapper.paper;

public class PaperSuggest {
    public String prefix = "";
    public int size = 5;

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        if(size>10)
            size = 10;
        if(size<0)
            size = 5;
        this.size = size;
    }
}
