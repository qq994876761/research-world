package com.lk.demo.entity.wrapper;

import java.util.ArrayList;

public class MultiSearchCondition {
    public int size = 20;
    public int page = 0;
    public ArrayList<Condition> must = new ArrayList<Condition>();
    public ArrayList<Condition> should = new ArrayList<Condition>();
    public ArrayList<Condition> must_not = new ArrayList<Condition>();

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public ArrayList<Condition> getMust() {
        return must;
    }

    public void setMust(ArrayList<Condition> must) {
        this.must = must;
    }

    public ArrayList<Condition> getShould() {
        return should;
    }

    public void setShould(ArrayList<Condition> should) {
        this.should = should;
    }

    public ArrayList<Condition> getMust_not() {
        return must_not;
    }

    public void setMust_not(ArrayList<Condition> must_not) {
        this.must_not = must_not;
    }
}
