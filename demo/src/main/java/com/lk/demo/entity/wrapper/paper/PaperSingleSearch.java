package com.lk.demo.entity.wrapper.paper;

public class PaperSingleSearch {
    private OneCondition condition;
    private Year year;
    //1,2,3,4分别表示：相关性，日期，被引量，浏览量从高到低，负数是从低到高。
    private Integer sort=1;
    private PaperFilter paperFilter=null;

    public OneCondition getCondition() {
        return condition;
    }

    public void setCondition(OneCondition condition) {
        this.condition = condition;
    }

    public Year getYear() {
        return year;
    }

    public void setYear(Year year) {
        this.year = year;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public PaperFilter getPaperFilter() {
        return paperFilter;
    }

    public void setPaperFilter(PaperFilter paperFilter) {
        this.paperFilter = paperFilter;
    }
}
